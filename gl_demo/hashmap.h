#ifndef HASHMAP_H
#define HASHMAP_H

#include <cstdint>
#include <cstring>

struct HashMap
{
  uint32_t size, capacity, valueSize;
  uint32_t *keys;
  void *values;
};

// NOTE: For best results use a capacity of a prime number.
inline void HashMapInitialize( HashMap* map, uint32_t* keys, void* values,
                        uint32_t capacity, uint32_t valueSize )
{
  map->size = 0;
  map->capacity = capacity;
  map->keys = keys;
  map->values = values;
  map->valueSize = valueSize;
}

#define HashMapAdd( MAP, KEY, VALUE ) HashMapAdd_( MAP, KEY, &VALUE )

// NOTE: 0 is an invalid key.
// TODO: Should really take sizeof value and check it to make sure that we
//       don't write more or less than we're suppose to.
inline uint32_t HashMapAdd_( HashMap *map, uint32_t key, void *value )
{
  if ( map->size < map->capacity )
  {
    uint32_t idx = key % map->capacity;
    int i = 0;
    int sign = 1;
    while ( map->keys[idx] != 0 )
    {
      if ( sign > 0 )
      {
        i++;
      }
      idx = ( key + sign * i * i ) % map->capacity;
      sign *= -1;
    }
    map->keys[idx] = key;
    memcpy( (uint8_t*)map->values + idx * map->valueSize, value,
            map->valueSize );
    map->size++;
    return key;
  }
  return 0;
}

#define HashMapGet( MAP, KEY, TYPE ) (TYPE*)HashMapGet_( MAP, KEY )

inline void* HashMapGet_( const HashMap *map, uint32_t key )
{
  uint32_t idx = key % map->capacity;
  int i = 0;
  int sign = 1;
  while ( map->keys[idx] != key )
  {
    if ( map->keys[idx] == 0 )
    {
      return nullptr;
    }
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( key + sign * i * i ) % map->capacity;
    sign *= -1;
  }
  return (uint8_t*)map->values + idx * map->valueSize;
}

// NOTE: Returns map->capacity if we're at the end.
inline uint32_t HashMapGetNextIndex( const HashMap* map, uint32_t index )
{
  for ( uint32_t i = index + 1; i < map->capacity; ++i )
  {
    if ( map->keys[i] != 0 )
    {
      return i;
    }
  }
  return map->capacity;
}

inline void HashMapRemove( HashMap *map, uint32_t key )
{
		uint32_t idx = key % map->capacity;
		int i = 0;
		int sign = 1;
		while ( map->keys[idx] != 0 )
		{
			if ( map->keys[idx] == key )
			{
				// TODO: Find the last key in the collision sequence and swap and delete.
			}
			if ( map->keys[idx] == 0 )
			{
				return; // Couldn't find entry stored with key.
			}
			if ( sign > 0 )
			{
				i++;
			}
			idx = ( key + sign * i * i ) % map->capacity;
			sign *= -1;
		}
}


#endif // HASHMAP_H
