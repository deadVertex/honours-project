#ifndef __FUZZY_LOGIC_H__
#define __FUZZY_LOGIC_H__

#include <math.h>
#include <set>
#include <stack>
#include <ctime>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>
#include <set>


using namespace std;

/////////////////////////////////////////////////////

#define MAX_NO_OF_INPUTS 5
#define MAX_NO_OF_INP_REGIONS 7
#define MAX_NO_OF_OUTPUT_VALUES 8

#define TOO_SMALL 1e-6

//Trapezoidal membership function types
typedef enum { regular_trapezoid, left_trapezoid, right_trapezoid } trapz_type;

//Input parameters
enum { in_theta, in_theta_dot, in_x, in_x_dot };

//Fuzzy sets
enum { in_neg, in_ze, in_pos };

//Fuzzy output terms
enum { out_nl, out_nm, out_ns, out_ze, out_ps, out_pm, out_pl };

enum { out_very_fast, out_fast, out_med, out_slow, out_very_slow };

enum { in_angle, in_distance };

enum { in_near, in_med, in_far, in_very_far };

enum { IN_NL, IN_NM, IN_NS, IN_ZE, IN_PS, IN_PM, IN_PL };

enum { out_sharp_left, out_mild_left, out_shallow_left, out_none, out_sharp_right, out_mild_right, out_shallow_right };


struct trapezoid {
  trapz_type tp;
  float a, b, c, d, l_slope, r_slope;

};

struct rule {
  short inp_index[ MAX_NO_OF_INPUTS ],
  inp_fuzzy_set[ MAX_NO_OF_INPUTS ],
  out_fuzzy_set;
};

struct fuzzy_system_rec {
  bool allocated;
  trapezoid inp_mem_fns[ MAX_NO_OF_INPUTS ][ MAX_NO_OF_INP_REGIONS ];
  rule *rules;
  int no_of_inputs, no_of_inp_regions, no_of_rules, no_of_outputs;
  float output_values[ MAX_NO_OF_OUTPUT_VALUES ];
};

extern fuzzy_system_rec g_fuzzy_system;
extern fuzzy_system_rec g_fuzzy_system_pursuit_speed;
extern fuzzy_system_rec g_fuzzy_system_pursuit_angle;
extern fuzzy_system_rec g_fuzzy_system_avoidance_speed;
extern fuzzy_system_rec g_fuzzy_system_avoidance_angle;

//---------------------------------------------------------------------------






/*  File FLPRCS.C */
trapezoid init_trapz( float x1, float x2, float x3, float x4, trapz_type typ );
float fuzzy_system( float inputs[ ], fuzzy_system_rec fl );
void free_fuzzy_rules( fuzzy_system_rec *fz );





//-------------------------------------------------------------------------

void initFuzzyRules( fuzzy_system_rec *fl );
void initMembershipFunctions( fuzzy_system_rec *fl );
void initFuzzySystem( fuzzy_system_rec *fl );



trapezoid init_trapz( float x1, float x2, float x3, float x4, trapz_type typ );
float trapz( float x, trapezoid trz );
float min_of( float values[ ], int no_of_inps );
float fuzzy_system( float inputs[ ], fuzzy_system_rec fz );
void free_fuzzy_rules( fuzzy_system_rec *fz );

void initFuzzySystemTargetPursuitSpeed( fuzzy_system_rec *fl );
void initFuzzySystemTargetPursuitAngle( fuzzy_system_rec *fl );

void initFuzzySystemAvoidanceSpeed( fuzzy_system_rec *fl );
void initFuzzySystemAvoidanceAngle( fuzzy_system_rec *fl );


#endif
