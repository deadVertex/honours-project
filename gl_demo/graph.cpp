#include "graph.h"

#include <algorithm>
#include <cassert>

Graph::Graph()
    : m_numConnections( 0 )
{
  HashMapInitialize( &m_hashMap, m_keys, m_values, MAX_NODES,
                     sizeof( ConnectionArray ) );
}

void Graph::AddConnection( uint32_t to, uint32_t from, float cost )
{
  Connection connection;
  connection.m_to = to;
  connection.m_from = from;
  connection.m_cost = cost;
  auto existingConnectionArray = HashMapGet( &m_hashMap, from, ConnectionArray );
  if ( existingConnectionArray )
  {
    for ( uint32_t i = 0; i < existingConnectionArray->numConnections; ++i )
    {
      if ( existingConnectionArray->connections[ i ].m_to == to )
      {
        // Ignore the duplicate.
        // TODO: Return some value to notify user.
        existingConnectionArray->connections[i] = connection;
        return;
      }
    }
    if ( existingConnectionArray->numConnections == MAX_CONNECTIONS_PER_NODE )
    {
        int a = 5;
        a++;
    }
    assert( existingConnectionArray->numConnections < MAX_CONNECTIONS_PER_NODE );
    existingConnectionArray->connections[
        existingConnectionArray->numConnections++ ] = connection;
    m_numConnections++;
    return;
  }
  ConnectionArray newConnectionArray;
  newConnectionArray.connections[0] = connection;
  newConnectionArray.numConnections = 1;
  uint32_t result = HashMapAdd( &m_hashMap, from, newConnectionArray );
  assert( result == from ); // If this is not the case then the hash map is full.
  m_numConnections++;
}

uint32_t Graph::GetConnections( Connection *connections, uint32_t size,
  uint32_t fromNode ) const
{
  auto connectionArray = HashMapGet( &m_hashMap, fromNode, ConnectionArray );
  if ( connectionArray )
  {
    auto n = std::min( size, connectionArray->numConnections );
    std::copy_n( connectionArray->connections, n, connections );
    return n;
  }
  return 0;
}

uint32_t Graph::GetDegree( uint32_t node ) const
{
  Connection connections[MAX_CONNECTIONS_PER_NODE];
  return GetConnections( connections, MAX_CONNECTIONS_PER_NODE, node );
}

uint32_t Graph::GetNumConnections() const
{
    return m_numConnections;
}

uint32_t Graph::GetAllConnections( Connection *connections,
                                   uint32_t size ) const
{
  uint32_t numConnections = 0;
  for ( uint32_t index = 0; index < MAX_NODES;
        index = HashMapGetNextIndex( &m_hashMap, index ) )
  {
    auto connectionArray = m_values + index;
    for ( uint32_t i = 0; i < connectionArray->numConnections; ++i )
    {
      if ( numConnections >= size )
      {
        return size;
      }
      connections[numConnections++] = connectionArray->connections[i];
    }
  }
  return numConnections;
}
