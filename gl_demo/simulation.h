#ifndef SIMULATION_H
#define SIMULATION_H

#include "agent.h"
#include "grid_world.h"
#include "graph.h"
#include "fuzzy_logic.h"
#include "algorithms.h"


struct Simulation
{
  GridWorld world;
  uint32_t numAgents;
  Agent agents[ 10 ];
  Agent *prey;

  fuzzy_system_rec fuzzy_system_pursuit_angle;
  fuzzy_system_rec fuzzy_system_pursuit_speed;
  fuzzy_system_rec fuzzy_system_avoidance_angle;
  fuzzy_system_rec fuzzy_system_avoidance_speed;

  uint32_t path[ 1024 ];
  int pathLength;
  uint32_t currentPathNode;
  uint32_t numFleeBeacons;
  uint32_t fleeBeacons[ 30 ];
  uint8_t *nodeCoverStates;
  float w;

  CoverElementHeap_t *coverElementHeap;
  AStarNodeHeap_t *astarNodeHeap;
  glm::vec2 rotation, rotationVelocity;
  GraphAbstractionTree tree;

  uint32_t numAbstractStates[ MAX_ABSTRACTION_LEVELS ];
  uint32_t numAbstractionLevels;
  Graph *graphLevels[ MAX_ABSTRACTION_LEVELS ];
  NodeGrid gridLevels[ MAX_ABSTRACTION_LEVELS ];
  AbstractState *abstractStates[ MAX_ABSTRACTION_LEVELS ];
};

extern bool InitializeSimulation( Simulation *simulation, const char *config );
extern void UpdateContainingNode( const NodeGrid &grid, Agent *agent );
extern bool CheckForInterception( Agent *agents, uint32_t numAgents );
extern int FindNodeFurthestAwayFromPredators( const NodeGrid &grid,
                                              uint32_t *nodes,
                                              uint32_t numNodes, Agent *agents,
                                              uint32_t numAgents );
extern void MovePredators( const Graph &graph, const NodeGrid &grid,
                           Agent *agents, uint32_t numAgents,
                           uint8_t *nodeCoverStates, uint32_t numNodes,
                           CoverElementHeap_t *queue,
                           AStarNodeHeap_t *astarQueue, float w,
                           const std::unordered_set< uint32_t > &allowedNodes );
extern void FuzzyMoveAgent( const NodeGrid &grid, Agent *agent, float dt,
                            fuzzy_system_rec *pursuitAngle,
                            fuzzy_system_rec *pursuitSpeed,
                            fuzzy_system_rec *avoidanceAngle,
                            fuzzy_system_rec *avoidanceSpeed);
#endif // SIMULATION_H
