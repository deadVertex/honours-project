#include <algorithm>

#include "fuzzy_logic.h"

/////////////////////////////////////////////////////////////////

//Initialise Fuzzy Rules

void initFuzzyRules( fuzzy_system_rec *fl ) {

  const int
    no_of_x_rules = 9,
    no_of_theta_rules = 9;

  int i;

  for ( i = 0; i < no_of_theta_rules; i++ ) {
    fl->rules[ i ].inp_index[ 0 ] = in_theta;
    fl->rules[ i ].inp_index[ 1 ] = in_theta_dot;
  }


  /* Regions for theta and theta_dot: */
  fl->rules[ 0 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 0 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 0 ].out_fuzzy_set = out_nl;
  fl->rules[ 1 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 1 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 1 ].out_fuzzy_set = out_nm;
  fl->rules[ 2 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 2 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 2 ].out_fuzzy_set = out_ze;
  fl->rules[ 3 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 3 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 3 ].out_fuzzy_set = out_nm;
  fl->rules[ 4 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 4 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 4 ].out_fuzzy_set = out_ze;
  fl->rules[ 5 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 5 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 5 ].out_fuzzy_set = out_pm;
  fl->rules[ 6 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 6 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 6 ].out_fuzzy_set = out_ze;
  fl->rules[ 7 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 7 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 7 ].out_fuzzy_set = out_pm;
  fl->rules[ 8 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 8 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 8 ].out_fuzzy_set = out_pl;

  for ( i = 0; i < no_of_x_rules; i++ ) {
    fl->rules[ i + no_of_theta_rules ].inp_index[ 0 ] = in_x;
    fl->rules[ i + no_of_theta_rules ].inp_index[ 1 ] = in_x_dot;
  }

  /* Regions for x and x_dot: */
  fl->rules[ 9 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 9 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 9 ].out_fuzzy_set = out_pl;
  fl->rules[ 10 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 10 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 10 ].out_fuzzy_set = out_ze;
  fl->rules[ 11 ].inp_fuzzy_set[ 0 ] = in_neg;
  fl->rules[ 11 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 11 ].out_fuzzy_set = out_ze;
  fl->rules[ 12 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 12 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 12 ].out_fuzzy_set = out_ze;
  fl->rules[ 13 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 13 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 13 ].out_fuzzy_set = out_ze;
  fl->rules[ 14 ].inp_fuzzy_set[ 0 ] = in_ze;
  fl->rules[ 14 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 14 ].out_fuzzy_set = out_ze;
  fl->rules[ 15 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 15 ].inp_fuzzy_set[ 1 ] = in_neg;
  fl->rules[ 15 ].out_fuzzy_set = out_ze;
  fl->rules[ 16 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 16 ].inp_fuzzy_set[ 1 ] = in_ze;
  fl->rules[ 16 ].out_fuzzy_set = out_ze;
  fl->rules[ 17 ].inp_fuzzy_set[ 0 ] = in_pos;
  fl->rules[ 17 ].inp_fuzzy_set[ 1 ] = in_pos;
  fl->rules[ 17 ].out_fuzzy_set = out_nl;
  return;
}

void initMembershipFunctions( fuzzy_system_rec *fl ) {
  /* The X membership functions */
  fl->inp_mem_fns[ in_x ][ in_neg ] = init_trapz( -1.5, -0.5, 0, 0, left_trapezoid );
  fl->inp_mem_fns[ in_x ][ in_ze ] = init_trapz( -1.5, -0.5, 0.5, 1.5, regular_trapezoid );
  fl->inp_mem_fns[ in_x ][ in_pos ] = init_trapz( 0.5, 1.5, 0, 0, right_trapezoid );
  /* The X dot membership functions */
  fl->inp_mem_fns[ in_x_dot ][ in_neg ] = init_trapz( -3.0, 0, 0, 0, left_trapezoid );
  fl->inp_mem_fns[ in_x_dot ][ in_ze ] = init_trapz( -1.5, -0.5, 0.5, 1.5, regular_trapezoid );
  fl->inp_mem_fns[ in_x_dot ][ in_pos ] = init_trapz( 0, 3.0, 0, 0, right_trapezoid );
  /* The theta membership functions */
  fl->inp_mem_fns[ in_theta ][ in_neg ] = init_trapz( -0.1, 0, 0, 0, left_trapezoid );
  fl->inp_mem_fns[ in_theta ][ in_ze ] = init_trapz( -0.1, -0.025, 0.025, 0.1, regular_trapezoid );
  fl->inp_mem_fns[ in_theta ][ in_pos ] = init_trapz( 0, 0.1, 0, 0, right_trapezoid );
  /* The theta dot membership functions */
  fl->inp_mem_fns[ in_theta_dot ][ in_neg ] = init_trapz( -0.1, 0, 0, 0, left_trapezoid );
  fl->inp_mem_fns[ in_theta_dot ][ in_ze ] = init_trapz( -0.15, -0.025, 0.025, 0.15, regular_trapezoid );
  fl->inp_mem_fns[ in_theta_dot ][ in_pos ] = init_trapz( 0, 0.1, 0, 0, right_trapezoid );
  return;
}

void initFuzzySystem( fuzzy_system_rec *fl ) {
  fl->no_of_inputs = 2;  /* Inputs are handled 2 at a time only */
  fl->no_of_rules = 18;
  fl->no_of_inp_regions = 3;
  fl->no_of_outputs = 5;
  fl->output_values[ out_nl ] = -10.0;
  fl->output_values[ out_nm ] = -5.0;
  fl->output_values[ out_ze ] = 0.0;
  fl->output_values[ out_pm ] = 5.0;
  fl->output_values[ out_pl ] = 10.0;
  fl->rules = ( rule * )malloc( ( size_t )( fl->no_of_rules*sizeof( rule ) ) );
  initFuzzyRules( fl );
  initMembershipFunctions( fl );
  return;
}

//////////////////////////////////////////////////////////////////////////////

trapezoid init_trapz( float x1, float x2, float x3, float x4, trapz_type typ ) {

  trapezoid trz;
  trz.a = x1;
  trz.b = x2;
  trz.c = x3;
  trz.d = x4;
  trz.tp = typ;
  switch ( trz.tp ) {

  case regular_trapezoid:
    trz.l_slope = 1.0 / ( trz.b - trz.a );
    trz.r_slope = 1.0 / ( trz.c - trz.d );
    break;

  case left_trapezoid:
    trz.r_slope = 1.0 / ( trz.a - trz.b );
    trz.l_slope = 0.0;
    break;

  case right_trapezoid:
    trz.l_slope = 1.0 / ( trz.b - trz.a );
    trz.r_slope = 0.0;
    break;
  }  /* end switch  */

  return trz;
}  /* end function */


//////////////////////////////////////////////////////////////////////////////
float trapz( float x, trapezoid trz ) {
  switch ( trz.tp ) {

  case left_trapezoid:
    if ( x <= trz.a )
      return 1.0;
    if ( x >= trz.b )
      return 0.0;
    /* a < x < b */
    return trz.r_slope * ( x - trz.b );


  case right_trapezoid:
    if ( x <= trz.a )
      return 0.0;
    if ( x >= trz.b )
      return 1.0;
    /* a < x < b */
    return trz.l_slope * ( x - trz.a );

  case regular_trapezoid:
    if ( ( x <= trz.a ) || ( x >= trz.d ) )
      return 0.0;
    if ( ( x >= trz.b ) && ( x <= trz.c ) )
      return 1.0;
    if ( ( x >= trz.a ) && ( x <= trz.b ) )
      return trz.l_slope * ( x - trz.a );
    if ( ( x >= trz.c ) && ( x <= trz.d ) )
      return  trz.r_slope * ( x - trz.d );

  }  /* End switch  */

  return 0.0;  /* should not get to this point */
}  /* End function */

//////////////////////////////////////////////////////////////////////////////
float min_of( float values[ ], int no_of_inps ) {
  int i;
  float val;
  val = values[ 0 ];
  for ( i = 1; i < no_of_inps; i++ ) {
    if ( values[ i ] < val )
      val = values[ i ];
  }
  return val;
}


//////////////////////////////////////////////////////////////////////////////
float fuzzy_system( float inputs[ ], fuzzy_system_rec fz ) {
  int i, j;
  short variable_index, fuzzy_set;
  float sum1 = 0.0, sum2 = 0.0, weight;
  float m_values[ MAX_NO_OF_INPUTS ];

  for ( i = 0; i < fz.no_of_rules; i++ ) {
    for ( j = 0; j < fz.no_of_inputs; j++ ) {
      variable_index = fz.rules[ i ].inp_index[ j ];
      fuzzy_set = fz.rules[ i ].inp_fuzzy_set[ j ];
      m_values[ j ] = trapz( inputs[ variable_index ],
        fz.inp_mem_fns[ variable_index ][ fuzzy_set ] );
    } /* end j  */
    weight = min_of( m_values, fz.no_of_inputs );
    sum1 += weight * fz.output_values[ fz.rules[ i ].out_fuzzy_set ];
    sum2 += weight;
  } /* end i  */
  if ( fabs( sum2 ) < TOO_SMALL ) {
    cout << "\r\nFLPRCS Error: Sum2 in fuzzy_system is 0.  Press key: " << endl;
    //getch();
    exit( 1 );
    return 0.0;
  }

  return ( sum1 / sum2 );
}  /* end fuzzy_system  */

//////////////////////////////////////////////////////////////////////////////
void free_fuzzy_rules( fuzzy_system_rec *fz ) {
  if ( fz->allocated ){
    free( fz->rules );
  }

  fz->allocated = false;
  return;
}


void initFuzzyRulesTargetPursuitSpeed( fuzzy_system_rec *fl )
{
  int i;
  for ( i = 0; i < fl->no_of_rules; i++ ) {
    fl->rules[ i ].inp_index[ 0 ] = in_angle;
    fl->rules[ i ].inp_index[ 1 ] = in_distance;
  }

  fl->rules[ 0 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 0 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 0 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 1 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 1 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 1 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 2 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 2 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 2 ].out_fuzzy_set = out_slow;

  fl->rules[ 3 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 3 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 3 ].out_fuzzy_set = out_med;

  fl->rules[ 4 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 4 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 4 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 5 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 5 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 5 ].out_fuzzy_set = out_slow;

  fl->rules[ 6 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 6 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 6 ].out_fuzzy_set = out_fast;

  fl->rules[ 7 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 7 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 7 ].out_fuzzy_set = out_very_fast;

  fl->rules[ 8 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 8 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 8 ].out_fuzzy_set = out_slow;

  fl->rules[ 9 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 9 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 9 ].out_fuzzy_set = out_med;

  fl->rules[ 10 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 10 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 10 ].out_fuzzy_set = out_fast;

  fl->rules[ 11 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 11 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 11 ].out_fuzzy_set = out_very_fast;

  fl->rules[ 12 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 12 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 12 ].out_fuzzy_set = out_med;

  fl->rules[ 13 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 13 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 13 ].out_fuzzy_set = out_fast;

  fl->rules[ 14 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 14 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 14 ].out_fuzzy_set = out_very_fast;

  fl->rules[ 15 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 15 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 15 ].out_fuzzy_set = out_very_fast;

  fl->rules[ 16 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 16 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 16 ].out_fuzzy_set = out_slow;

  fl->rules[ 17 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 17 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 17 ].out_fuzzy_set = out_med;

  fl->rules[ 18 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 18 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 18 ].out_fuzzy_set = out_fast;

  fl->rules[ 19 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 19 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 19 ].out_fuzzy_set = out_very_fast;

  fl->rules[ 20 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 20 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 20 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 21 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 21 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 21 ].out_fuzzy_set = out_slow;

  fl->rules[ 22 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 22 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 22 ].out_fuzzy_set = out_med;

  fl->rules[ 23 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 23 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 23 ].out_fuzzy_set = out_fast;

  fl->rules[ 24 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 24 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 24 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 25 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 25 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 25 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 26 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 26 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 26 ].out_fuzzy_set = out_slow;

  fl->rules[ 27 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 27 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 27 ].out_fuzzy_set = out_med;
}

void initMembershipFunctionsPursuitSpeed( fuzzy_system_rec *fl )
{
  fl->inp_mem_fns[ in_distance ][ in_near ] = init_trapz( 0.0f, 0.5f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_med ] = init_trapz( 0.2f, 0.6f, 0.8f, 1.2f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_far ] = init_trapz( 0.8f, 1.2f, 1.4f, 1.8f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_very_far ] = init_trapz( 1.8f, 2.0f, 0.0f, 0.0f, right_trapezoid );

  fl->inp_mem_fns[ in_angle ][ IN_NL ] = init_trapz( -90.0f, -60.0f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NM ] = init_trapz( -65.0f, -50.0f, -30.0f, -20.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NS ] = init_trapz( -25.0f, -10.0f, -5.0f, -1.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_ZE ] = init_trapz( -5.0f, -2.0f, 2.0f, 5.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PS ] = init_trapz( 1.0f, 5.0f, 10.0f, 25.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PM ] = init_trapz( 20.0f, 30.0f, 50.0f, 65.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PL ] = init_trapz( 60.0f, 90.0f, 0.0f, 0.0f, right_trapezoid );
}

void initFuzzySystemTargetPursuitSpeed( fuzzy_system_rec *fl )
{
  fl->no_of_inputs = 2;
  fl->no_of_inp_regions = 7;
  fl->no_of_rules = 28;
  fl->no_of_outputs = 5;

  fl->output_values[ out_very_slow ] = 0.25f;
  fl->output_values[ out_slow ] = 0.6f;
  fl->output_values[ out_med ] = 0.8f;
  fl->output_values[ out_fast ] = 1.2f;
  fl->output_values[ out_very_fast ] = 1.6f;

  fl->rules = new rule[ fl->no_of_rules ];
  fl->allocated = true;

  initFuzzyRulesTargetPursuitSpeed( fl );
  initMembershipFunctionsPursuitSpeed( fl );
}

void initFuzzyRulesTargetPursuitAngle( fuzzy_system_rec *fl )
{
  int i;
  for ( i = 0; i < fl->no_of_rules; i++ ) {
    fl->rules[ i ].inp_index[ 0 ] = in_angle;
    fl->rules[ i ].inp_index[ 1 ] = in_distance;
  }

  fl->rules[ 0 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 0 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 0 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 1 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 1 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 1 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 2 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 2 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 2 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 3 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 3 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 3 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 4 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 4 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 4 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 5 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 5 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 5 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 6 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 6 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 6 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 7 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 7 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 7 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 8 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 8 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 8 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 9 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 9 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 9 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 10 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 10 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 10 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 11 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 11 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 11 ].out_fuzzy_set = out_none;

  fl->rules[ 12 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 12 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 12 ].out_fuzzy_set = out_none;

  fl->rules[ 13 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 13 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 13 ].out_fuzzy_set = out_none;

  fl->rules[ 14 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 14 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 14 ].out_fuzzy_set = out_none;

  fl->rules[ 15 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 15 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 15 ].out_fuzzy_set = out_none;

  fl->rules[ 16 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 16 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 16 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 17 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 17 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 17 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 18 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 18 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 18 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 19 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 19 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 19 ].out_fuzzy_set = out_none;

  fl->rules[ 20 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 20 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 20 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 21 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 21 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 21 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 22 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 22 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 22 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 23 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 23 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 23 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 24 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 24 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 24 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 25 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 25 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 25 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 26 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 26 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 26 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 27 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 27 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 27 ].out_fuzzy_set = out_shallow_right;
}

void initMembershipFunctionsTargetPursuitAngle( fuzzy_system_rec *fl )
{
  fl->inp_mem_fns[ in_distance ][ in_near ] = init_trapz( 0.0f, 0.5f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_med ] = init_trapz( 0.2f, 0.6f, 0.8f, 1.2f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_far ] = init_trapz( 0.8f, 1.2f, 1.4f, 1.8f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_very_far ] = init_trapz( 1.8f, 2.0f, 0.0f, 0.0f, right_trapezoid );

  /*fl->inp_mem_fns[ in_distance ][ in_near ] = init_trapz( 14.5f, 16.0f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_med ] = init_trapz( 12.0f, 15.0f, 18.0f, 20.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_far ] = init_trapz( 17.5f, 19.5f, 23.5f, 25.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_very_far ] = init_trapz( 24.5f, 26.0f, 0.0f, 0.0f, right_trapezoid );*/

  fl->inp_mem_fns[ in_angle ][ IN_NL ] = init_trapz( -90.0f, -60.0f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NM ] = init_trapz( -65.0f, -50.0f, -30.0f, -20.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NS ] = init_trapz( -25.0f, -10.0f, -5.0f, -1.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_ZE ] = init_trapz( -5.0f, -2.0f, 2.0f, 5.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PS ] = init_trapz( 1.0f, 5.0f, 10.0f, 25.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PM ] = init_trapz( 20.0f, 30.0f, 50.0f, 65.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PL ] = init_trapz( 60.0f, 90.0f, 0.0f, 0.0f, right_trapezoid );
}

void initFuzzySystemTargetPursuitAngle( fuzzy_system_rec *fl )
{
  fl->no_of_inputs = 2;
  fl->no_of_inp_regions = 7;
  fl->no_of_rules = 28;
  fl->no_of_outputs = 5;

  fl->output_values[ out_none ] = 0.0f;
  fl->output_values[ out_shallow_left ] = 2.0f;
  fl->output_values[ out_mild_left ] = 5.0f;
  fl->output_values[ out_sharp_left ] = 7.5f;
  fl->output_values[ out_shallow_right ] = -2.0f;
  fl->output_values[ out_mild_right ] = -5.0f;
  fl->output_values[ out_sharp_right ] = -7.5f;

  fl->rules = new rule[ fl->no_of_rules ];
  fl->allocated = true;

  initFuzzyRulesTargetPursuitAngle( fl );
  initMembershipFunctionsTargetPursuitAngle( fl );
}

void initFuzzyRulesAvoidanceAngle( fuzzy_system_rec *fl )
{
  int i;
  for ( i = 0; i < fl->no_of_rules; i++ ) {
    fl->rules[ i ].inp_index[ 0 ] = in_angle;
    fl->rules[ i ].inp_index[ 1 ] = in_distance;
  }

  fl->rules[ 0 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 0 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 0 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 1 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 1 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 1 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 2 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 2 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 2 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 3 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 3 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 3 ].out_fuzzy_set = out_none;

  fl->rules[ 4 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 4 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 4 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 5 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 5 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 5 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 6 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 6 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 6 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 7 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 7 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 7 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 8 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 8 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 8 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 9 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 9 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 9 ].out_fuzzy_set = out_sharp_right;

  fl->rules[ 10 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 10 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 10 ].out_fuzzy_set = out_mild_right;

  fl->rules[ 11 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 11 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 11 ].out_fuzzy_set = out_shallow_right;

  fl->rules[ 12 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 12 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 12 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 13 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 13 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 13 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 14 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 14 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 14 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 15 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 15 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 15 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 16 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 16 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 16 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 17 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 17 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 17 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 18 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 18 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 18 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 19 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 19 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 19 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 20 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 20 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 20 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 21 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 21 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 21 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 22 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 22 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 22 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 23 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 23 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 23 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 24 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 24 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 24 ].out_fuzzy_set = out_sharp_left;

  fl->rules[ 25 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 25 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 25 ].out_fuzzy_set = out_mild_left;

  fl->rules[ 26 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 26 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 26 ].out_fuzzy_set = out_shallow_left;

  fl->rules[ 27 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 27 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 27 ].out_fuzzy_set = out_none;
}

void initMembershipFunctionsAvoidanceAngle( fuzzy_system_rec *fl )
{
  fl->inp_mem_fns[ in_distance ][ in_near ] = init_trapz( 0.0f, 0.5f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_med ] = init_trapz( 0.2f, 0.6f, 0.8f, 1.2f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_far ] = init_trapz( 0.8f, 1.2f, 1.4f, 1.8f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_very_far ] = init_trapz( 1.8f, 2.0f, 0.0f, 0.0f, right_trapezoid );

  fl->inp_mem_fns[ in_angle ][ IN_NL ] = init_trapz( -90.0f, -60.0f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NM ] = init_trapz( -65.0f, -50.0f, -30.0f, -20.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NS ] = init_trapz( -25.0f, -10.0f, -5.0f, -1.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_ZE ] = init_trapz( -5.0f, -2.0f, 2.0f, 5.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PS ] = init_trapz( 1.0f, 5.0f, 10.0f, 25.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PM ] = init_trapz( 20.0f, 30.0f, 50.0f, 65.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PL ] = init_trapz( 60.0f, 90.0f, 0.0f, 0.0f, right_trapezoid );
}

void initFuzzySystemAvoidanceAngle( fuzzy_system_rec *fl )
{
  fl->no_of_inputs = 2;
  fl->no_of_inp_regions = 7;
  fl->no_of_rules = 28;
  fl->no_of_outputs = 5;

  //fl->output_values[ out_none ] = 0.0f;
  //fl->output_values[ out_shallow_left ] = 3.0f;
  //fl->output_values[ out_mild_left ] = 10.0f;
  //fl->output_values[ out_sharp_left ] = 20.0f;
  //fl->output_values[ out_shallow_right ] = -3.0f;
  //fl->output_values[ out_mild_right ] = -10.0f;
  //fl->output_values[ out_sharp_right ] = -20.0f;

  fl->output_values[ out_none ] = 0.0f;
  fl->output_values[ out_shallow_left ] = 2.0f;
  fl->output_values[ out_mild_left ] = 5.0f;
  fl->output_values[ out_sharp_left ] = 7.5f;
  fl->output_values[ out_shallow_right ] = -2.0f;
  fl->output_values[ out_mild_right ] = -5.0f;
  fl->output_values[ out_sharp_right ] = -7.5f;

  fl->rules = new rule[ fl->no_of_rules ];
  fl->allocated = true;

  initFuzzyRulesAvoidanceAngle( fl );
  initMembershipFunctionsAvoidanceAngle( fl );
}

void initFuzzyRulesAvoidanceSpeed( fuzzy_system_rec *fl )
{
  int i;
  for ( i = 0; i < fl->no_of_rules; i++ ) {
    fl->rules[ i ].inp_index[ 0 ] = in_angle;
    fl->rules[ i ].inp_index[ 1 ] = in_distance;
  }

  fl->rules[ 0 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 0 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 0 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 1 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 1 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 1 ].out_fuzzy_set = out_slow;

  fl->rules[ 2 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 2 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 2 ].out_fuzzy_set = out_med;

  fl->rules[ 3 ].inp_fuzzy_set[ 0 ] = IN_NL;
  fl->rules[ 3 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 3 ].out_fuzzy_set = out_fast;

  fl->rules[ 4 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 4 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 4 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 5 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 5 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 5 ].out_fuzzy_set = out_slow;

  fl->rules[ 6 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 6 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 6 ].out_fuzzy_set = out_med;

  fl->rules[ 7 ].inp_fuzzy_set[ 0 ] = IN_NM;
  fl->rules[ 7 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 7 ].out_fuzzy_set = out_fast;

  fl->rules[ 8 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 8 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 8 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 9 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 9 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 9 ].out_fuzzy_set = out_slow;

  fl->rules[ 10 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 10 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 10 ].out_fuzzy_set = out_med;

  fl->rules[ 11 ].inp_fuzzy_set[ 0 ] = IN_NS;
  fl->rules[ 11 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 11 ].out_fuzzy_set = out_fast;

  fl->rules[ 12 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 12 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 12 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 13 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 13 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 13 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 14 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 14 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 14 ].out_fuzzy_set = out_slow;

  fl->rules[ 15 ].inp_fuzzy_set[ 0 ] = IN_ZE;
  fl->rules[ 15 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 15 ].out_fuzzy_set = out_fast;

  fl->rules[ 16 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 16 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 16 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 17 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 17 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 17 ].out_fuzzy_set = out_slow;

  fl->rules[ 18 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 18 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 18 ].out_fuzzy_set = out_med;

  fl->rules[ 19 ].inp_fuzzy_set[ 0 ] = IN_PS;
  fl->rules[ 19 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 19 ].out_fuzzy_set = out_fast;

  fl->rules[ 20 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 20 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 20 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 21 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 21 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 21 ].out_fuzzy_set = out_slow;

  fl->rules[ 22 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 22 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 22 ].out_fuzzy_set = out_med;

  fl->rules[ 23 ].inp_fuzzy_set[ 0 ] = IN_PM;
  fl->rules[ 23 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 23 ].out_fuzzy_set = out_fast;

  fl->rules[ 24 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 24 ].inp_fuzzy_set[ 1 ] = in_near;
  fl->rules[ 24 ].out_fuzzy_set = out_very_slow;

  fl->rules[ 25 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 25 ].inp_fuzzy_set[ 1 ] = in_med;
  fl->rules[ 25 ].out_fuzzy_set = out_slow;

  fl->rules[ 26 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 26 ].inp_fuzzy_set[ 1 ] = in_far;
  fl->rules[ 26 ].out_fuzzy_set = out_med;

  fl->rules[ 27 ].inp_fuzzy_set[ 0 ] = IN_PL;
  fl->rules[ 27 ].inp_fuzzy_set[ 1 ] = in_very_far;
  fl->rules[ 27 ].out_fuzzy_set = out_fast;
}

void initMembershipFunctionsAvoidanceSpeed( fuzzy_system_rec *fl )
{
  fl->inp_mem_fns[ in_distance ][ in_near ] = init_trapz( 0.0f, 0.5f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_med ] = init_trapz( 0.2f, 0.6f, 0.8f, 1.2f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_far ] = init_trapz( 0.8f, 1.2f, 1.4f, 1.8f, regular_trapezoid );
  fl->inp_mem_fns[ in_distance ][ in_very_far ] = init_trapz( 1.8f, 2.0f, 0.0f, 0.0f, right_trapezoid );

  fl->inp_mem_fns[ in_angle ][ IN_NL ] = init_trapz( -90.0f, -60.0f, 0.0f, 0.0f, left_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NM ] = init_trapz( -65.0f, -50.0f, -30.0f, -20.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_NS ] = init_trapz( -25.0f, -10.0f, -5.0f, -1.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_ZE ] = init_trapz( -5.0f, -2.0f, 2.0f, 5.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PS ] = init_trapz( 1.0f, 5.0f, 10.0f, 25.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PM ] = init_trapz( 20.0f, 30.0f, 50.0f, 65.0f, regular_trapezoid );
  fl->inp_mem_fns[ in_angle ][ IN_PL ] = init_trapz( 60.0f, 90.0f, 0.0f, 0.0f, right_trapezoid );
}

void initFuzzySystemAvoidanceSpeed( fuzzy_system_rec *fl )
{
  fl->no_of_inputs = 2;
  fl->no_of_inp_regions = 7;
  fl->no_of_rules = 28;
  fl->no_of_outputs = 5;

  fl->output_values[ out_very_slow ] = 0.15f;
  fl->output_values[ out_slow ] = 0.3f;
  fl->output_values[ out_med ] = 0.45f;
  fl->output_values[ out_fast ] = 0.6f;
  fl->output_values[ out_very_fast ] = 0.75f;

  fl->rules = new rule[ fl->no_of_rules ];
  fl->allocated = true;

  initFuzzyRulesAvoidanceSpeed( fl );
  initMembershipFunctionsAvoidanceSpeed( fl );
}
