#ifndef __ALGORITHMS_H__
#define __ALGORITHMS_H__

#include <array>
#include <cassert>
#include <set>
#include <unordered_set>

#include "graph.h"
#include "agent.h"

// Based heavily on this example code.
// http://www.cprogramming.com/tutorial/computersciencetheory/heapcode.html

template< typename T, uint32_t Size, typename Compare >
class Heap
{
public:
  Heap();

  void Push( const T &val );
  T Pop();

  unsigned int GetSize() const { return m_current; }
  bool IsEmpty() const { return GetSize() == 0; }

  void Clear() { m_current = 0; }

  void Replace( const T &val );

private:
  static uint32_t ParentOf( uint32_t node ) { return ( node - 1 ) / 2; }
  static uint32_t LeftChildOf( uint32_t node ) { return ( node * 2 ) + 1; }

  void ShiftUp( uint32_t node );
  void ShiftDown( uint32_t node );

private:
  std::array< T, Size > m_array;
  uint32_t m_current;
  Compare m_compare;
};

struct CoverElement
{
  uint32_t location;
  Agent agent;
  float time;
};

struct CompareCoverElements
{
  bool operator()( const CoverElement &a, const CoverElement &b ) const
  {
    return a.time > b.time;
  }
};

struct AStarNode
{
  uint32_t node;
  std::vector< uint32_t > path;
  float hcost, fcost;

  bool operator <( const AStarNode &n ) const
  {
    return fcost < n.fcost;
  }

  bool operator ==( const AStarNode &n ) const
  {
    return node == n.node;
  }
};

struct CompareAStarNodes
{
  bool operator()( const AStarNode &a, const AStarNode &b ) const
  {
    return a.fcost > b.fcost;
  }
};

typedef Heap< CoverElement, 0xFFFF, CompareCoverElements > CoverElementHeap_t;
typedef Heap< AStarNode, 0xFFFF, CompareAStarNodes > AStarNodeHeap_t;

extern void CalculateCover( const Graph &graph, const Agent *agents,
  uint32_t numAgents, uint8_t *nodeCoverStates, uint32_t numNodes,
  CoverElementHeap_t *queue );

extern int AStar( const Graph &graph, const NodeGrid &grid,
  uint32_t *pathNodes, uint32_t maxPathSize,
  uint32_t startNode, uint32_t goalNode, AStarNodeHeap_t *queue,
  const std::unordered_set< uint32_t > &allowedNodes );

extern bool FindClique( uint32_t node, const Graph &graph,
                 uint32_t *clique, uint32_t cliqueSize,
                 const std::set< uint32_t > &availableNodes );

extern int PartialRefinementAStar(Graph **graphs, NodeGrid *grids,
    uint32_t numLevels, uint32_t *pathNodes, uint32_t maxPathSize,
    uint32_t startNode, uint32_t goalNode, AStarNodeHeap_t *queue,
                                  const GraphAbstractionTree& tree,
                                  std::unordered_set<uint32_t> *searchSpace );

const uint32_t MAX_NODES_PER_ABSTRACT_NODE = 16;
struct AbstractState
{
    uint32_t node;
    uint32_t children[MAX_NODES_PER_ABSTRACT_NODE];
    uint32_t numChildren;
    glm::vec2 position;
};
extern uint32_t AbstractGraph(const Graph &graph, const NodeGrid &nodeGrid,
    AbstractState *states, uint32_t size, Graph *abstractGraph,
    NodeGrid *abstractGrid , GraphAbstractionTree* tree, uint32_t level );




template< typename T, uint32_t Size, typename Compare >
Heap< T, Size, Compare >::Heap()
: m_current( 0 )
{
}

template< typename T, uint32_t Size, typename Compare >
void Heap< T, Size, Compare >::Push( const T &val )
{
  assert( m_current < Size );
  m_array[ m_current ] = val;
  ShiftUp( m_current++ );
}

template< typename T, uint32_t Size, typename Compare >
T Heap< T, Size, Compare >::Pop()
{
  T element = m_array[ 0 ];
  m_current--;
  m_array[ 0 ] = m_array[ m_current ];
  ShiftDown( 0 );
  return element;
}

template< typename T, uint32_t Size, typename Compare >
void Heap< T, Size, Compare >::Replace( const T &val )
{
  for ( uint32_t i = 0; i < m_current; ++i )
  {
    if ( m_array[ i ] == val )
    {
      if ( m_compare( m_array[ i ], val ) )
      {
        --m_current;
        m_array[ i ] = m_array[ m_current ];
        ShiftDown( i );
        break;
      }
      return;
    }
  }
  Push( val );
}

template< typename T, uint32_t Size, typename Compare >
void Heap< T, Size, Compare >::ShiftUp( uint32_t node )
{
  uint32_t current = node;
  T element = m_array[ current ];

  while ( current > 0 )
  {
    int parent = ParentOf( current );
    if ( m_compare( m_array[ parent ], element ) )
    {
      m_array[ current ] = m_array[ parent ];
      current = parent;
    }
    else
      break;
  }
  m_array[ current ] = element;
}

template< typename T, uint32_t Size, typename Compare >
void Heap< T, Size, Compare >::ShiftDown( uint32_t node )
{
  uint32_t current = node;
  uint32_t child = LeftChildOf( current );
  T el = m_array[ current ];
  while ( child < m_current )
  {
    if ( child < ( m_current - 1 ) )
    {
      if ( m_compare( m_array[ child ], m_array[ child + 1 ] ) )
        child++;
    }

    if ( m_compare( el, m_array[ child ] ) )
    {
      m_array[ current ] = m_array[ child ];
      current = child;
      child = LeftChildOf( current );
    }
    else
      break;
  }
  m_array[ current ] = el;
}


#endif
