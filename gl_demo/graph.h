#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <vector>
#include <unordered_map>
#include <stdint.h>
#include <cassert>

#include "hashmap.h"

static const uint32_t MAX_NODES = 300000;
static const uint32_t MAX_CONNECTIONS_PER_NODE = 8;
static const uint32_t MAX_WIDTH = 8192;
static const uint32_t MAX_HEIGHT = 8192;
static const uint32_t MAX_ABSTRACTION_LEVELS = 32;
static const uint32_t MAX_ABSTRACT_STATES = 100000;

class Graph
{
public:
  struct Connection
  {
    float m_cost;
    uint32_t m_to, m_from;
  };
  struct ConnectionArray
  {
    uint32_t numConnections;
    Connection connections[MAX_CONNECTIONS_PER_NODE];

    ConnectionArray() : numConnections( 0 ) {}
  };

public:
  Graph();

  void AddConnection( uint32_t to, uint32_t from, float cost );

  uint32_t GetConnections( Connection *connections, uint32_t size,
    uint32_t fromNode ) const;

  uint32_t GetNumConnections() const;

  uint32_t GetAllConnections( Connection *connections, uint32_t size ) const;

  uint32_t GetDegree( uint32_t node ) const;

private:
//  std::unordered_map< uint32_t, ConnectionArray > m_connections;
  uint32_t m_keys[MAX_NODES];
  ConnectionArray m_values[MAX_NODES];
  HashMap m_hashMap;
  uint32_t m_numConnections;
};

class NodeGrid
{
public:
  NodeGrid();
  inline uint32_t AddNode( uint32_t row, uint32_t col );
  inline bool
  GetNodePosition( uint32_t *row, uint32_t *col, uint32_t node ) const;
  inline bool
  GetNodeAtPosition( uint32_t row, uint32_t col, uint32_t *node ) const;

  inline void
  SetNodePosition( uint32_t row, uint32_t col, uint32_t node ) const;

  inline uint32_t GetNumNodes() const;

  inline uint32_t GetXMin() const { return m_xMin; }
  inline uint32_t GetYMin() const { return m_yMin; }
  inline uint32_t GetXMax() const { return m_xMax; }
  inline uint32_t GetYMax() const { return m_yMax; }

private:
  std::vector< std::pair< uint16_t, uint16_t > > m_nodePositions;
  HashMap m_hashMap;
  uint32_t m_keys[MAX_NODES];
  std::size_t m_values[MAX_NODES];
//  std::unordered_map< std::pair< uint32_t, uint32_t >, uint32_t > m_nodeMap;
  uint32_t m_xMin, m_yMin, m_xMax, m_yMax;
};

class GraphAbstractionTree
{
public:
  inline void InitializeLevel( uint32_t level,uint32_t size );
  inline void AddNode( uint32_t node, uint32_t parent, uint32_t level );
  inline uint32_t GetParent( uint32_t node, uint32_t level ) const;
  inline uint32_t GetChildren( uint32_t node, uint32_t level,
                               uint32_t *children, uint32_t size ) const;

private:
  std::vector< uint32_t > m_parentArray[MAX_ABSTRACTION_LEVELS];
};

inline NodeGrid::NodeGrid()
    : m_xMin( 0xFFFFFFFF ), m_yMin( 0xFFFFFFFF ), m_xMax( 0 ), m_yMax( 0 )
{
    HashMapInitialize( &m_hashMap, m_keys, m_values, MAX_NODES,
                       sizeof( uint32_t ) );
    m_nodePositions.reserve( 200000 );
}

inline uint32_t NodeGrid::AddNode( uint32_t row, uint32_t col )
{
  assert( ( row < MAX_WIDTH ) && ( col < MAX_HEIGHT) );
  m_nodePositions.push_back( std::make_pair<uint16_t,uint16_t>( row, col ) );
  m_xMin = std::min( row, m_xMin );
  m_xMax = std::max( row, m_xMax );
  m_yMin = std::min( col, m_yMin );
  m_yMax = std::max( col, m_yMax );
  uint32_t key = ( ( row + 1 ) << 16 ) | ( col + 1 );
  std::size_t value = m_nodePositions.size() - 1;
  assert( key != 0 );
  HashMapAdd( &m_hashMap, key, value );
  return m_nodePositions.size() - 1;
}

inline bool NodeGrid::GetNodePosition(
  uint32_t *row, uint32_t *col, uint32_t node ) const
{
  if ( node < m_nodePositions.size() )
  {
      auto &pos = m_nodePositions[ node ];
      *row = (uint32_t)pos.first;
      *col = (uint32_t)pos.second;
      return true;
  }
  return false;
}

inline bool NodeGrid::GetNodeAtPosition(
  uint32_t row, uint32_t col, uint32_t *node ) const
{
  assert( ( row < MAX_WIDTH) && ( col <= MAX_HEIGHT) );
  uint32_t key = ( ( row + 1 ) << 16 ) | ( col + 1 );
  assert( key != 0 );
  std::size_t* value = HashMapGet( &m_hashMap, key, std::size_t );
  if ( value )
  {
      *node = *value;
      return true;
  }
  return false;
}

//inline void NodeGrid::SetNodePosition( uint32_t row, uint32_t col,
//                                       uint32_t node )
//{
//  assert( ( row < MAX_WIDTH ) && ( col < MAX_HEIGHT) );
//  assert( node < m_nodePositions.size() );
//  m_nodePositions[node] = std::make_pair<uint16_t, uint16_t>( row, col );
//  m_xMin = std::min( row, m_xMin );
//  m_xMax = std::max( row, m_xMax );
//  m_yMin = std::min( col, m_yMin );
//  m_yMax = std::max( col, m_yMax );
//  uint32_t key = ( ( row + 1 ) << 16 ) | ( col + 1 );
//  std::size_t value = m_nodePositions.size() - 1;
//  assert( key != 0 );
//  // TODO: Remove previous entry from hash table.
//  HashMapAdd( &m_hashMap, key, value );
//}

inline uint32_t NodeGrid::GetNumNodes() const
{
  return m_nodePositions.size();
}

inline void GraphAbstractionTree::InitializeLevel( uint32_t level,
                                                   uint32_t size )
{
  assert( level < MAX_ABSTRACTION_LEVELS );
  m_parentArray[level].resize( size, 0 );
}

inline void GraphAbstractionTree::AddNode( uint32_t node, uint32_t parent,
                                           uint32_t level )
{
  assert( level < MAX_ABSTRACTION_LEVELS );
  assert( node < m_parentArray[level].size() );
  m_parentArray[level][node] = parent;
}

inline uint32_t GraphAbstractionTree::GetParent( uint32_t node,
                                                 uint32_t level ) const
{
  assert( level < MAX_ABSTRACTION_LEVELS );
  assert( node < m_parentArray[level].size() );
  return m_parentArray[level][node];
}

inline uint32_t GraphAbstractionTree::GetChildren( uint32_t node,
                                                   uint32_t level,
                                                   uint32_t *children,
                                                   uint32_t size ) const
{
  assert( level < MAX_ABSTRACTION_LEVELS );
  uint32_t numChildren = 0;
  for ( uint32_t i = 0; i < m_parentArray[level].size(); ++i )
  {
    if ( m_parentArray[level][i] == node )
    {
      if ( numChildren < size )
      {
        children[numChildren++] = i;
      }
      else
      {
        break;
      }
    }
  }
  return numChildren;
}

#endif // !__GRAPH_H__
