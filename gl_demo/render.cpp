#include "render.h"

#include <cstring>

#include "glm/gtc/type_ptr.hpp"

static const GLuint VERTEX_POSITION = 0;
static const GLuint VERTEX_NORMAL = 1;
static const GLuint VERTEX_TEXTURE_COORD = 2;

static bool CompileShader(GLuint shader, const char* src, bool isVertex,
                          ShaderErrorCallback_t callback )
{
    int len = strlen(src);
    glShaderSource(shader, 1, &src, &len);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        char buffer[ 2048 ];
        char *cursor = nullptr;
        int len = 0;
        if ( isVertex )
        {
            cursor = strcpy( buffer, "[VERTEX]\n" );
        }
        else
        {
            cursor = strcpy( buffer, "[FRAGMENT]\n" );
        }
        glGetShaderInfoLog( shader, cursor - buffer, &len, cursor );
        callback( buffer );
        return false;
    }
    return true;
}

static bool LinkShader(GLuint vertex, GLuint fragment, GLuint program,
                       ShaderErrorCallback_t callback )
{
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        char buffer[ 2048 ];
        int len = 0;
        char *cursor = strcpy( buffer, "[LINKER]\n");
        glGetProgramInfoLog( program, cursor - buffer, &len, cursor );
        callback( buffer );
        return false;
    }
    return true;
}

void DeleteShader( GLuint program )
{
    glUseProgram( 0 );
    GLsizei count = 0;
    GLuint shaders[ 2 ];
    glGetAttachedShaders( program, 2, &count, shaders );
    glDeleteProgram( program );
    glDeleteShader( shaders[ 0 ] );
    glDeleteShader( shaders[ 1 ] );
}

GLuint
CreateShader( const char *vertexSource,
              const char *fragmentSource,
              ShaderErrorCallback_t callback )
{
    GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
    GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
    GLuint program = glCreateProgram();

    bool vertexSuccess = CompileShader( vertex, vertexSource, true, callback );
    bool fragmentSuccess = CompileShader( fragment, fragmentSource, false,
                                          callback );

    if ( vertexSuccess && fragmentSuccess )
    {
      if ( LinkShader( vertex, fragment, program, callback ) )
      {
        return program;
      }
    }
    DeleteShader( program );
    return 0;
}

GLBufferedMesh CreateBufferedMesh( float *vertices, uint32_t vertexSize,
                                   uint32_t numVertices )
{
    GLBufferedMesh mesh = {};
    mesh.numVertices = numVertices;
    glGenVertexArrays( 1, &mesh.vertexArray );
    glBindVertexArray( mesh.vertexArray );

    glGenBuffers( 1, &mesh.vertexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, mesh.vertexBuffer );

    glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                  GL_STATIC_DRAW );

    glEnableVertexAttribArray( VERTEX_POSITION );

    glBindBuffer( GL_ARRAY_BUFFER, mesh.vertexBuffer );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE,
                           vertexSize, NULL );

    glBindVertexArray( 0 );
    return mesh;
}

GLBufferedMesh CreateBufferedMesh( glm::vec3 *vertices, uint32_t numVertices )
{
    return CreateBufferedMesh( glm::value_ptr( *vertices ), sizeof( glm::vec3 ),
                               numVertices );
}


GLBufferedMesh CreateTriangle()
{
    GLBufferedMesh tri = {};
    tri.numVertices = 3;
    glGenVertexArrays( 1, &tri.vertexArray );
    glBindVertexArray( tri.vertexArray );

    glGenBuffers( 1, &tri.vertexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, tri.vertexBuffer );

    float vertices[] = {
      -0.5f, -0.5f, 0.0f,
      1.0f, 0.0f, 0.0f,

      0.5f, -0.5f, 0.0f,
      0.0f, 1.0f, 0.0f,

      0.0f,  0.5f, 0.0f,
      0.0f, 0.0f, 1.0f
    };

    glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices,
                  GL_STATIC_DRAW );

    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    glBindBuffer( GL_ARRAY_BUFFER, tri.vertexBuffer );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE,
                           sizeof( float ) * 6, NULL );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_TRUE,
                           sizeof( float ) * 6,
                           (char*)( sizeof( float ) * 3 ));

    glBindVertexArray( 0 );
    return tri;
}

GLBufferedMesh CreateQuad()
{

    float vertices[] = {
        0.5f, -0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
    };
    return CreateBufferedMesh( vertices, sizeof( float ) * 3,
                               sizeof( vertices ) / ( sizeof( float ) * 3 ) );
}

GLIndexedMesh CreateCube()
{
    GLIndexedMesh cube = {};
    cube.numIndices = 36;
    glGenVertexArrays( 1, &cube.vertexArray );
    glBindVertexArray( cube.vertexArray );

    glGenBuffers( 1, &cube.vertexBuffer );
    glGenBuffers( 1, &cube.indexBuffer );

    float vertices[] = {
      // Top
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

      // Bottom
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,

      // Back
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,

      // Front
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

      // Left
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,

      // Right
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };

    GLuint indices[] = {
      0, 1, 2,
      2, 3, 0,

      4, 5, 6,
      6, 7, 4,

      8, 9, 10,
      10, 11, 8,

      12, 13, 14,
      14, 15, 12,

      16, 17, 18,
      18, 19, 16,

      20, 21, 22,
      22, 23, 20
    };

    glBindBuffer( GL_ARRAY_BUFFER, cube.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices,
                  GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, cube.indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( indices ), indices,
      GL_STATIC_DRAW );

    glBindBuffer( GL_ARRAY_BUFFER, cube.vertexBuffer );

    glEnableVertexAttribArray( VERTEX_POSITION );
    glEnableVertexAttribArray( VERTEX_NORMAL );
    glEnableVertexAttribArray( VERTEX_TEXTURE_COORD );

    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE,
      sizeof( float ) * 8, NULL );

    glVertexAttribPointer( VERTEX_NORMAL, 3, GL_FLOAT, GL_TRUE,
      sizeof( float ) * 8, (char*)( sizeof( float ) * 3 ) );

    glVertexAttribPointer( VERTEX_TEXTURE_COORD, 2, GL_FLOAT, GL_FALSE,
      sizeof( float ) * 8, (char*)( sizeof( float ) * 6 ) );

    glBindVertexArray( 0 );
    return cube;
}

void DeleteMesh( GLBufferedMesh mesh )
{
    glDeleteBuffers( 1, &mesh.vertexBuffer );
    glDeleteVertexArrays( 1, &mesh.vertexArray );
}

inline glm::vec3 CreateCirclePoint( float segment, float radius )
{
  float angle = segment * glm::pi< float >() * 2.0f;
  glm::vec3 v( glm::cos( angle ), glm::sin( angle ), 0.0f );
  return ( v * radius );
}

struct BuildCircleResult
{
    uint32_t numVertices, numIndices;
};

// TODO: Array bounds checking
static BuildCircleResult BuildCircle( glm::vec3 *vertices, uint32_t *indices,
                                      uint32_t vertexOffset,
                                      glm::vec3 center, float radius,
                                      int segments, bool fill )
{
    uint32_t numVertices = vertexOffset;
    uint32_t numIndices = 0;

    if ( fill )
    {
        vertices[ numVertices++ ] = center;
    }
    for ( int segment = 0; segment < segments; ++segment )
    {
        indices[ numIndices++ ] = numVertices;
        float t = (float)segment / segments;
        vertices[ numVertices++ ] = center + CreateCirclePoint( t, radius );
        if ( fill )
        {
            indices[ numIndices++ ] = vertexOffset;
        }
        indices[ numIndices++ ] = numVertices;
    }
    if ( fill )
    {
        indices[ numIndices - 1 ] = vertexOffset + 1;
    }
    else
    {
      indices[ numIndices - 1 ] = vertexOffset;
    }

    BuildCircleResult result;
    result.numVertices = numVertices - vertexOffset;
    result.numIndices = numIndices;
    return result;
}

GLIndexedMesh CreateCircle( float radius, int segments, bool fill )
{
    glm::vec3 vertices[ 2048 ];
    uint32_t indices[ 2048 ];

    auto result = BuildCircle( vertices, indices, 0, glm::vec3(), radius,
                               segments, fill );

    GLIndexedMesh circle = {};
    circle.numIndices = result.numIndices;
    glGenVertexArrays( 1, &circle.vertexArray );
    glBindVertexArray( circle.vertexArray );

    glGenBuffers( 1, &circle.vertexBuffer );
    glGenBuffers( 1, &circle.indexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, circle.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec3 ) * result.numVertices,
                  vertices, GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, circle.indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                  sizeof( uint32_t ) * result.numIndices, indices,
                  GL_STATIC_DRAW );

    glBindBuffer( GL_ARRAY_BUFFER, circle.vertexBuffer );

    glEnableVertexAttribArray( VERTEX_POSITION );

    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE,
      sizeof( glm::vec3 ), NULL );

    glBindVertexArray( 0 );
    return circle;
}

void DrawPath( uint32_t *path, uint32_t pathLength,
               const NodeGrid &grid, const glm::mat4 &viewProjection,
               const glm::vec4 &colour, GLBufferedMesh pathMesh,
               GLint matrixLocation, GLint colourLocation )
{
    if ( pathLength == 0 )
    {
      return;
    }
    uint32_t numVertices = 0;
    glm::vec3 vertices[ 1024 ];
    for ( uint32_t i = 0; i < pathLength - 1; ++i )
    {
        uint32_t x1, y1, x2, y2;
        grid.GetNodePosition( &x1, &y1, path[ i ] );
        grid.GetNodePosition( &x2, &y2, path[ i + 1 ] );

        glm::vec3 *v1 = vertices + numVertices++;
        v1->x = x1 + 0.5f;
        v1->y = y1 + 0.5f;
        glm::vec3 *v2 = vertices + numVertices++;
        v2->x = x2 + 0.5f;
        v2->y = y2 + 0.5f;
    }
    glBindVertexArray( 0 );
    glBindBuffer( GL_ARRAY_BUFFER, pathMesh.vertexBuffer );
    GLsizei size = numVertices * sizeof( float ) * 3;
    glEnableVertexAttribArray( VERTEX_POSITION );
    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
      nullptr );

    glBufferSubData( GL_ARRAY_BUFFER, 0, size, vertices );
		glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
		                    glm::value_ptr( viewProjection ) );
		glUniform4fv( colourLocation, 1, glm::value_ptr( colour ) );
    glDrawArrays( GL_LINES, 0, numVertices );
}

GLIndexedMesh CreateGraphConnectionsVisualization( const Graph &graph,
                                                   const NodeGrid &grid )
{
    glm::vec3 *vertices = new glm::vec3[grid.GetNumNodes()];
    uint32_t *indices = new uint32_t[graph.GetNumConnections()*2];

    uint32_t numIndices = 0;

    for ( uint32_t node = 0; node < grid.GetNumNodes(); ++node )
    {
        uint32_t row, col;
        if ( grid.GetNodePosition( &row, &col, node ) )
        {
            vertices[ node ] = glm::vec3( row + 0.5f, col + 0.5f, 0.0f );

            Graph::Connection connections[ 8 ];
            auto numConnections = graph.GetConnections( connections, 8, node );
            for ( uint32_t connectionIdx = 0;
                  connectionIdx < numConnections;
                  ++connectionIdx )
            {
              uint32_t otherNode = connections[ connectionIdx ].m_to;
              indices[ numIndices++ ] = node;
              indices[ numIndices++ ] = otherNode;
            }
        }
    }

    GLIndexedMesh visualization = {};
    visualization.numIndices = numIndices;
    glGenVertexArrays( 1, &visualization.vertexArray );
    glBindVertexArray( visualization.vertexArray );

    glGenBuffers( 1, &visualization.vertexBuffer );
    glGenBuffers( 1, &visualization.indexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, visualization.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER,
                  sizeof( glm::vec3 ) * grid.GetNumNodes(),
                  vertices, GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, visualization.indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
                  indices, GL_STATIC_DRAW );

    glBindBuffer( GL_ARRAY_BUFFER, visualization.vertexBuffer );

    glEnableVertexAttribArray( VERTEX_POSITION );

    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL );

    glBindVertexArray( 0 );
    delete[] vertices;
    delete[] indices;
    return visualization;
}
GLIndexedMesh CreateGraphNodesVisualization( const Graph &graph,
                                             const NodeGrid &grid )
{
    glm::vec3 *vertices = new glm::vec3[grid.GetNumNodes()*9];
    uint32_t *indices = new uint32_t[grid.GetNumNodes()*24];

    uint32_t numVertices = 0;
    uint32_t numIndices = 0;

    for ( uint32_t node = 0; node < grid.GetNumNodes(); ++node )
    {
        uint32_t row, col;
        if ( grid.GetNodePosition( &row, &col, node ) )
        {
            auto circle = BuildCircle( vertices,
                                       indices + numIndices, numVertices,
                                       glm::vec3( row + 0.5f, col + 0.5f, 0.0f ), 0.1f, 8,
                                       true );
            numVertices += circle.numVertices;
            numIndices += circle.numIndices;
        }
    }

    GLIndexedMesh visualization = {};
    visualization.numIndices = numIndices;
    glGenVertexArrays( 1, &visualization.vertexArray );
    glBindVertexArray( visualization.vertexArray );

    glGenBuffers( 1, &visualization.vertexBuffer );
    glGenBuffers( 1, &visualization.indexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, visualization.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec3 ) * numVertices,
                  vertices, GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, visualization.indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
                  indices, GL_STATIC_DRAW );

    glBindBuffer( GL_ARRAY_BUFFER, visualization.vertexBuffer );

    glEnableVertexAttribArray( VERTEX_POSITION );

    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL );
    delete[] vertices;
    delete[] indices;
    glBindVertexArray( 0 );
    return visualization;
}

StreamingVertexBuffer CreateStreamingVertexBuffer( uint32_t maxVertices )
{
    StreamingVertexBuffer buffer = {};
    buffer.numVertices = 0;
    buffer.maxVertices = maxVertices;

    glGenVertexArrays( 1, &buffer.vertexArray );
    glBindVertexArray( buffer.vertexArray );

    glGenBuffers( 1, &buffer.vertexBuffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec3 ) * maxVertices,
                  nullptr, GL_STREAM_DRAW );

    glEnableVertexAttribArray( VERTEX_POSITION );
    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE,
                           sizeof( glm::vec3 ), NULL );
    glBindVertexArray( 0 );
    return buffer;
}

GLIndexedMesh CreateIndexedMesh( glm::vec3 *vertices, uint32_t numVertices,
                                        uint32_t *indices, uint32_t numIndices )
{
    GLIndexedMesh mesh = {};
    mesh.numIndices = numIndices;
    glGenVertexArrays( 1, &mesh.vertexArray );
    glBindVertexArray( mesh.vertexArray );

    glGenBuffers( 1, &mesh.vertexBuffer );
    glGenBuffers( 1, &mesh.indexBuffer );

    glBindBuffer( GL_ARRAY_BUFFER, mesh.vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, numVertices * sizeof( glm::vec3 ), vertices,
                  GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof( uint32_t ),
                  indices, GL_STATIC_DRAW );

    glBindBuffer( GL_ARRAY_BUFFER, mesh.vertexBuffer );

    glEnableVertexAttribArray( VERTEX_POSITION );

    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL );

    glBindVertexArray( 0 );
    return mesh;
}
