#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "glwidget.h"

#include <sstream>

#include "global_variables.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QGLFormat format = QGLFormat::defaultFormat();
    format.setSampleBuffers( true );
    format.setSampleBuffers( 4 );
    QGLFormat::setDefaultFormat( format );
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    zoomFactor = value;
}

void MainWindow::on_pushButton_2_toggled(bool checked)
{
   followPrey = checked;
}

void MainWindow::on_horizontalSlider_2_valueChanged(int value)
{
   w = (float)value / 100.0f;
   std::stringstream ss;
   ss << w;
   this->ui->w_display->setText( ss.str().c_str() );
}

void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
   displayedAbstractionLevel = value;
}

void MainWindow::on_draw_path_toggled(bool checked)
{
   drawPath = checked;
}

void MainWindow::on_draw_cover_states_toggled(bool checked)
{
   drawCoverStates = checked;
}

void MainWindow::on_draw_graph_toggled(bool checked)
{
   drawGraph = checked;
}
