#-------------------------------------------------
#
# Project created by QtCreator 2015-02-08T23:18:32
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gl_demo
TEMPLATE = app

QMAKE_CXXFLAGS += -g -std=c++11
QMAKE_CXXFLAGS_RELEASE += -O4

SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    grid_world.cpp \
    graph.cpp \
    algorithms.cpp \
    fuzzy_logic.cpp \
    simulation.cpp \
    render.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    grid_world.h \
    agent.h \
    graph.h \
    algorithms.h \
    fuzzy_logic.h \
    simulation.h \
    global_variables.h \
    render.h \
    hashmap.h

FORMS    += mainwindow.ui

Release:DESTDIR = ../build/release
Release:OBJECTS_DIR = ../build/release/.obj
Release:MOC_DIR = ../build/release/.moc
Release:RCC_DIR = ../build/release/.rcc
Release:UI_DIR = ../build/release/.ui

Debug:DESTDIR = ../build/debug
Debug:OBJECTS_DIR = ../build/debug/.obj
Debug:MOC_DIR = ../build/debug/.moc
Debug:RCC_DIR = ../build/debug/.rcc
Debug:UI_DIR = ../build/debug/.ui
unix:!macx: LIBS += -L$$PWD/ -lGLEW

DEPENDPATH += $$PWD/

unix:!macx: PRE_TARGETDEPS += $$PWD/libGLEW.a

INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/

win32: LIBS += -L$$PWD/ -llibglew32
