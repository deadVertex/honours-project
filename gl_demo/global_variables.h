#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

extern int zoomFactor;
extern bool followPrey;
extern bool followPredators;
extern float w;
extern int displayedAbstractionLevel;
extern bool drawPath;
extern bool drawCoverStates;
extern bool drawGraph;

#endif // GLOBAL_VARIABLES_H
