#define GLEW_STATIC
#include "GL/glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/compatibility.hpp"

#include <QMessageBox>
#include <QKeyEvent>

#include <cstdio>
#include <cstdarg>
#include <cstdint>
#include <queue>
#include <sstream>

#include "glwidget.h"
#include "agent.h"
#include "grid_world.h"
#include "graph.h"
#include "fuzzy_logic.h"
#include "algorithms.h"
#include "global_variables.h"
#include "simulation.h"
#include "render.h"

float g_predatorSpeed = 1.0f;
float g_preySpeed = 2.0f;

int zoomFactor = 1.0f;
bool followPrey = false;
bool followPredators = false;
float w = 0.0f;
int displayedAbstractionLevel = 0;
bool drawPath = false;
bool drawCoverStates = false;
bool drawGraph = false;

GLWidget::GLWidget( QWidget* parent ) : QGLWidget( parent )
{
  connect( &timer, SIGNAL( timeout() ), this, SLOT( updateGL() ) );
  timer.start( 16 );
}

static void MsgBox( const char* fmt, ... )
{
  char buffer[4096];
  va_list list;
  va_start( list, fmt );
  vsprintf( buffer, fmt, list );
  QMessageBox msgBox;
  msgBox.setText( buffer );
  msgBox.exec();
  va_end( list );
}

static bool CheckOpenGL()
{
  return GLEW_ARB_vertex_program && GLEW_ARB_fragment_program &&
         GLEW_ARB_vertex_buffer_object && GLEW_ARB_vertex_array_object;
}

static GLuint shader, colourShader;
static GLint matrixLocation, colourLocation, colourMatrixLocation;
static GLBufferedMesh mesh;
static GLIndexedMesh cube;
static GLBufferedMesh quad;
static float dt = 1.0f / 30.0f;
static uint32_t windowWidth, windowHeight;

static float cameraSpeed = 45.0f;
static glm::vec2 cameraPosition, cameraVelocity;

static Simulation* simulation;
static GLBufferedMesh pathMesh;
static GLIndexedMesh circle;
static GLIndexedMesh graphNodes, graphConnections;
static GLIndexedMesh abstractEdges[MAX_ABSTRACTION_LEVELS];
static std::unordered_set<uint32_t> allNodes;
static GLIndexedMesh gridWorldObstaclesMesh;
static GLBufferedMesh gridWorldTilesMesh;

static void OnShaderError( const char* msg ) { MsgBox( msg ); }

static GLIndexedMesh CreateAbstractEdgesMesh( const Graph& graph,
                                              AbstractState* abstractStates,
                                              uint32_t numAbstractStates )
{
  glm::vec3* vertices = new glm::vec3[numAbstractStates];
  for ( uint32_t i = 0; i < numAbstractStates; ++i )
  {
    AbstractState* state = abstractStates + i;
    vertices[i] =
      glm::vec3{state->position.x + 0.5f, state->position.y + 0.5f, 0.0f};
  }

  Graph::Connection* connections =
    new Graph::Connection[graph.GetNumConnections()];
  graph.GetAllConnections( connections, graph.GetNumConnections() );
  uint32_t* indices = new uint32_t[graph.GetNumConnections() * 2];
  for ( uint32_t i = 0; i < graph.GetNumConnections(); ++i )
  {
    Graph::Connection* connection = connections + i;
    indices[i * 2] = connection->m_from;
    indices[i * 2 + 1] = connection->m_to;
  }
  delete[] connections;
  auto mesh = CreateIndexedMesh( vertices, numAbstractStates, indices,
                                 graph.GetNumConnections() * 2 );
  delete[] vertices;
  delete[] indices;
  return mesh;
}

static GLIndexedMesh BuildGridWorldObstacleMesh( const GridWorld& world )
{
  uint32_t maxVertices = world.m_width * world.m_height * 8;
  uint32_t maxIndices = world.m_width * world.m_height * 36;
  glm::vec3* vertices = new glm::vec3[maxVertices];
  uint32_t* indices = new uint32_t[maxIndices];

  assert( vertices );
  uint32_t numVertices = 0;
  uint32_t numIndices = 0;
  for ( uint32_t y = 0; y < world.m_height; ++y )
  {
    for ( uint32_t x = 0; x < world.m_width; ++x )
    {
      uint32_t cellIndex = x + ( y * world.m_width );
      if ( ( world.m_cells[cellIndex].m_blocked ) ||
           ( x == world.m_width - 1 ) || ( y == world.m_height - 1 ) )
      {
        uint32_t localIndices[] = {0, 2, 1, 0, 3, 2,

                                   1, 2, 6, 6, 5, 1,

                                   4, 5, 6, 6, 7, 4,

                                   2, 3, 6, 6, 3, 7,

                                   0, 7, 3, 0, 4, 7,

                                   0, 1, 5, 0, 5, 4};
        for ( uint32_t i = 0; i < 36; ++i )
        {
          localIndices[i] += numVertices;
        }
        assert( ( numIndices + 36 ) < maxIndices );
        std::copy_n( localIndices, 36, indices + numIndices );
        numIndices += 36;

        glm::vec3 positions[] = {
          glm::vec3{-0.5, -0.5, 0.5},  glm::vec3{0.5, -0.5, 0.5},
          glm::vec3{0.5, 0.5, 0.5},    glm::vec3{-0.5, 0.5, 0.5},

          glm::vec3{-0.5, -0.5, -0.5}, glm::vec3{0.5, -0.5, -0.5},
          glm::vec3{0.5, 0.5, -0.5},   glm::vec3{-0.5, 0.5, -0.5}};

        assert( ( numVertices + 8 ) < maxVertices );
        for ( uint32_t i = 0; i < 8; ++i )
        {
          vertices[numVertices++] =
            positions[i] + glm::vec3( x + 0.5f, y + 0.5f, 0 );
        }
      }
    }
  }
  GLIndexedMesh mesh =
    CreateIndexedMesh( vertices, numVertices, indices, numIndices );
  delete[] vertices;
  delete[] indices;
  return mesh;
}

static GLBufferedMesh BuildGridWorldTilesMesh( const GridWorld& world )
{
  uint32_t maxVertices = ( world.m_width * 2 ) + ( world.m_height * 2 );
  glm::vec3* vertices = new glm::vec3[maxVertices];
  uint32_t numVertices = 0;
  for ( uint32_t x = 0; x < world.m_width; ++x )
  {
    vertices[numVertices++] = glm::vec3( x, 0, 0 );
    vertices[numVertices++] = glm::vec3( x, world.m_height, 0 );
  }
  for ( uint32_t y = 0; y < world.m_height; ++y )
  {
    vertices[numVertices++] = glm::vec3( 0, y, 0 );
    vertices[numVertices++] = glm::vec3( world.m_width, y, 0 );
  }
  GLBufferedMesh mesh = CreateBufferedMesh( vertices, numVertices );
  delete[] vertices;
  return mesh;
}

void GLWidget::initializeGL()
{
  GLenum status = glewInit();
  if ( status != GLEW_OK )
  {
    MsgBox( "Failed to initialize GLEW!" );
  }

  if ( !CheckOpenGL() )
  {
    MsgBox( "Wrong version of openGL." );
  }

  glEnable( GL_MULTISAMPLE );

  const char* vertexSource =
    "#version 330\n"
    "layout(location = 0) in vec3 position;\n"
    "uniform mat4 in_mCombined;\n"
    "void main()\n"
    "{\n"
    "	gl_Position = in_mCombined * vec4( position, 1.0);\n"
    "}";

  const char* fragmentSource = "#version 330\n"
                               "uniform vec4 colour;\n"
                               "out vec4 out_vColour;\n"
                               "void main()\n"
                               "{\n"
                               "out_vColour = colour;\n"
                               "}";

  const char* colourVertexSource =
    "#version 330\n"
    "layout(location = 0) in vec3 position;\n"
    "layout(location = 4) in vec3 vertexColour;\n"
    "uniform mat4 in_mCombined;\n"
    "out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "	 gl_Position = in_mCombined * vec4( position, 1.0);\n"
    "  colour = vec4( vertexColour, 1.0 );\n"
    "}";

  const char* colourFragmentSource =
    "#version 330\n"
    "in vec4 colour;\n"
    "out vec4 out_vColour;\n"
    "void main()\n"
    "{\n"
    "  out_vColour = vec4( 1.0, 0.0, 0.0, 1.0 );\n"
    "}";

  shader = CreateShader( vertexSource, fragmentSource, OnShaderError );
  colourShader =
    CreateShader( colourVertexSource, colourFragmentSource, OnShaderError );
  matrixLocation = glGetUniformLocation( shader, "in_mCombined" );
  colourLocation = glGetUniformLocation( shader, "colour" );
  colourMatrixLocation = glGetUniformLocation( colourShader, "in_mCombined" );
  mesh = CreateTriangle();
  cube = CreateCube();
  quad = CreateQuad();
  circle = CreateCircle( 0.5f, 8, false );
  glClearColor( 0.2, 0.2, 0.2, 1 );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_BLEND );
  glDepthFunc( GL_LESS );
  //  const char *config = "abstraction_test.dat";
  //  const char *config = "circle3.dat";
  const char* config = "../gl_demo/maps/maze512-32-0.map";
  uint64_t k = sizeof( Simulation );
  simulation = new Simulation;
  bool success = InitializeSimulation( simulation, config );
  assert( success );
  for ( uint32_t i = 0; i < simulation->gridLevels[0].GetNumNodes(); ++i )
  {
    allNodes.insert( i );
  }

  graphNodes = CreateGraphNodesVisualization( *simulation->graphLevels[0],
                                              simulation->gridLevels[0] );
  graphConnections = CreateGraphConnectionsVisualization(
    *simulation->graphLevels[0], simulation->gridLevels[0] );

  for ( uint32_t i = 1; i < simulation->numAbstractionLevels; ++i )
  {
    abstractEdges[i] = CreateAbstractEdgesMesh(
      *( simulation->graphLevels[i] ), simulation->abstractStates[i],
      simulation->numAbstractStates[i] );
  }

  gridWorldObstaclesMesh = BuildGridWorldObstacleMesh( simulation->world );
  gridWorldTilesMesh = BuildGridWorldTilesMesh( simulation->world );
  cameraPosition =
    glm::vec2{simulation->world.m_width / 2, simulation->world.m_height / 2};

  glBindVertexArray( 0 );
  glGenBuffers( 1, &pathMesh.vertexBuffer );
  glBindBuffer( GL_ARRAY_BUFFER, pathMesh.vertexBuffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec3 ) * 2048, NULL,
                GL_STREAM_DRAW );
}

static void RenderAgents( const glm::mat4& viewProjection, uint32_t numAgents,
                          Agent* agents )
{
  for ( uint32_t agentIndex = 0; agentIndex < numAgents; ++agentIndex )
  {
    Agent* agent = agents + agentIndex;
    glm::mat4 model;
    model = glm::translate(
      model, glm::vec3( agent->position.x, agent->position.y, 0.0 ) );
    model = glm::scale( model, glm::vec3( 0.25, 0.25, 0.5 ) );
    glm::vec4 test{0, 0, 0, 1};
    glm::mat4 combined = viewProjection * model;
    test = combined * test;
    test /= test.w;
    glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                        glm::value_ptr( combined ) );
    if ( agent->type == agent_types::PREDATOR )
    {
      glUniform4f( colourLocation, 1.0f, 0.0f, 0.0f, 1.0f );
    }
    else
    {
      glUniform4f( colourLocation, 0.0f, 0.0f, 1.0f, 1.0f );
    }
    glBindVertexArray( cube.vertexArray );
    glDrawElements( GL_TRIANGLES, cube.numIndices, GL_UNSIGNED_INT, NULL );
  }
}

static void RenderGridWorld( const GridWorld& world,
                             const glm::mat4& viewProjection, float scale )
{
  glBindVertexArray( gridWorldObstaclesMesh.vertexArray );
  glUniform4f( colourLocation, 0.1f, 0.1f, 0.1f, 1.0f );
  glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( viewProjection ) );
  glDrawElements( GL_TRIANGLES, gridWorldObstaclesMesh.numIndices,
                  GL_UNSIGNED_INT, NULL );

  glBindVertexArray( gridWorldTilesMesh.vertexArray );
  glDrawArrays( GL_LINES, 0, gridWorldTilesMesh.numVertices );
}

static void RenderCoverStates( Simulation* simulation,
                               const glm::mat4& viewProjection )
{
  CalculateCover( *simulation->graphLevels[0], simulation->agents,
                  simulation->numAgents, simulation->nodeCoverStates,
                  simulation->gridLevels[0].GetNumNodes(),
                  simulation->coverElementHeap );

  glBindVertexArray( quad.vertexArray );
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  for ( uint32_t node = 0; node < simulation->gridLevels[0].GetNumNodes();
        ++node )
  {
    uint32_t row, col;
    simulation->gridLevels[0].GetNodePosition( &row, &col, node );

    glm::mat4 model =
      glm::translate( glm::mat4(), glm::vec3( row + 0.5f, col + 0.5f, 0 ) );
    glm::mat4 combined = viewProjection * model;
    glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                        glm::value_ptr( combined ) );
    if ( simulation->nodeCoverStates[node] == node_cover_states::COVERED )
    {
      glUniform4f( colourLocation, 1.0f, 135.0f / 255.0f, 0.0f, 0.5f );
    }
    else if ( simulation->nodeCoverStates[node] ==
              node_cover_states::TARGET_COVERED )
    {
      glUniform4f( colourLocation, 0.0f, 135.0f / 255.0f, 249.0f / 255.0f,
                   0.5f );
    }

    glDrawArrays( GL_QUADS, 0, quad.numVertices );
  }
}

static glm::vec4 colours[] = {
  glm::vec4{1.0f, 0.0f, 0.0f, 1.0f}, glm::vec4{0.0f, 1.0f, 0.0f, 1.0f},
  glm::vec4{0.0f, 0.0f, 1.0f, 1.0f}, glm::vec4{1.0f, 1.0f, 0.0f, 1.0f},
  glm::vec4{0.0f, 1.0f, 1.0f, 1.0f}, glm::vec4{1.0f, 0.0f, 1.0f, 1.0f},
  glm::vec4{1.0f, 1.0f, 1.0f, 1.0f}};

static const uint32_t numColours = sizeof( colours ) / sizeof( glm::vec4 );
static uint32_t colourIndex = 0;

static void DrawNodes( const NodeGrid& grid, const glm::mat4& viewProjection )
{
  glBindVertexArray( circle.vertexArray );
  auto start = 0;
  for ( uint32_t node = start; node < start + grid.GetNumNodes(); ++node )
  {
    uint32_t row, col;
    grid.GetNodePosition( &row, &col, node );
    glm::mat4 model;
    model = glm::translate( model, glm::vec3( row + 0.5f, -0.5f, col + 0.5f ) );
    model = glm::scale( model, glm::vec3( 0.25f, 1, 0.25f ) );
    glm::mat4 combined = viewProjection * model;
    glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                        glm::value_ptr( combined ) );
    glm::vec4 colour( 0.1f, 0.1f, 0.1f, 1.0f );
    glUniform4fv( colourLocation, 1, glm::value_ptr( colours[colourIndex] ) );
    colourIndex = ( colourIndex + 1 ) % numColours;
    glDrawElements( GL_LINES, circle.numIndices, GL_UNSIGNED_INT, NULL );
  }
}

static AbstractState* GetAbstractState( Simulation* simulation, uint32_t level,
                                        uint32_t nodeId )
{
  uint32_t numStates = simulation->numAbstractStates[level];
  for ( uint32_t i = 0; i < numStates; ++i )
  {
    auto state = simulation->abstractStates[level] + i;
    if ( state->node == nodeId )
    {
      return state;
    }
  }
  return nullptr;
}

static void DrawAbstractState( Simulation* simulation, uint32_t level,
                               AbstractState* state,
                               const glm::mat4& viewProjection )
{
  for ( uint32_t i = 0; i < state->numChildren; ++i )
  {
    auto node = state->children[i];
    if ( level > 0 )
    {
      auto childState = GetAbstractState( simulation, level, node );
      //      assert( childState );
      if ( childState )
      {
        DrawAbstractState( simulation, level - 1, childState, viewProjection );
      }
    }
    else
    {
      uint32_t row, col;
      simulation->gridLevels[0].GetNodePosition( &row, &col, node );
      glm::mat4 model;
      model =
        glm::translate( model, glm::vec3( row + 0.5f, col + 0.5f, 0.0f ) );
      glm::mat4 combined = viewProjection * model;
      glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                          glm::value_ptr( combined ) );
      glDrawArrays( GL_QUADS, 0, quad.numVertices );
    }
  }
}
static void DrawCliques( Simulation* simulation, uint32_t level,
                         const glm::mat4& viewProjection )
{
  glBindVertexArray( quad.vertexArray );
  for ( uint32_t i = 0; i < simulation->numAbstractStates[level]; ++i )
  {
    auto state = simulation->abstractStates[level] + i;
    float t = (float)i / (float)simulation->numAbstractStates[level];
    glm::vec4 colour( t, ( ( i + 13 ) % 41 ) / 41.0f, ( ( i + 5 ) % 7 ) / 7.0f,
                      1.0f );
    glUniform4fv( colourLocation, 1, glm::value_ptr( colour ) );
    DrawAbstractState( simulation, level - 1, state, viewProjection );
  }
}

static void DrawAbstractEdges( uint32_t level, const glm::mat4& viewProjection )
{
  glBindVertexArray( abstractEdges[level].vertexArray );
  glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( viewProjection ) );
  glm::vec4 colour{1.0f, 0.0f, 1.0f, 1.0f};
  glUniform4fv( colourLocation, 1, glm::value_ptr( colour ) );
  glDrawElements( GL_LINES, abstractEdges[level].numIndices, GL_UNSIGNED_INT,
                  NULL );
}

static void DrawGraph( const glm::mat4& viewProjection )
{
  glm::vec4 colour{0.5f, 0.9f, 0.0f, 1.0f};
  glUniform4fv( colourLocation, 1, glm::value_ptr( colour ) );
  glBindVertexArray( graphNodes.vertexArray );
  glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( viewProjection ) );
  glDrawElements( GL_TRIANGLES, graphNodes.numIndices, GL_UNSIGNED_INT, NULL );
  glBindVertexArray( graphConnections.vertexArray );
  glDrawElements( GL_LINES, graphConnections.numIndices, GL_UNSIGNED_INT,
                  NULL );
}

void GLWidget::paintGL()
{
  for ( uint32_t i = 0; i < simulation->numAgents; ++i )
  {
    UpdateContainingNode( simulation->gridLevels[0], simulation->agents + i );
  }

  if ( CheckForInterception( simulation->agents, simulation->numAgents ) )
  {
    // MsgBox("Prey has been caught.");
    // TODO: Quit
  }

  if ( simulation->pathLength > 0 )
  {
    uint32_t px, py;
    simulation->gridLevels[0].GetNodePosition(
      &px, &py, simulation->path[simulation->currentPathNode] );
    glm::vec2 targetPosition( px + 0.5f, py + 0.5f );
    if ( simulation->prey->targetNode == simulation->prey->containingNode )
    {
      if ( simulation->currentPathNode < simulation->pathLength - 1 )
      {
        simulation->currentPathNode++;
        simulation->gridLevels[0].GetNodePosition(
          &px, &py, simulation->path[simulation->currentPathNode] );
        simulation->prey->targetNode =
          simulation->path[simulation->currentPathNode];
        targetPosition = glm::vec2( px + 0.5f, py + 0.5f );
      }
    }
  }

  std::unordered_set<uint32_t> searchSpace;
  if ( simulation->prey->containingNode == simulation->prey->targetNode ||
       simulation->currentPathNode > 3 )
  {
    int index = FindNodeFurthestAwayFromPredators(
      simulation->gridLevels[0], simulation->fleeBeacons,
      simulation->numFleeBeacons, simulation->agents, simulation->numAgents );

    assert( index >= 0 );
    uint32_t goalNode = simulation->fleeBeacons[index];

    simulation->pathLength =
      PartialRefinementAStar( simulation->graphLevels, simulation->gridLevels,
                              simulation->numAbstractionLevels,
                              simulation->path, 1024,
                              simulation->prey->containingNode, goalNode,
                              simulation->astarNodeHeap, simulation->tree,
                              &searchSpace );

    if ( simulation->pathLength < 0 )
    {
      std::exit( -1 );
    }
    simulation->currentPathNode = 0;
    if ( simulation->pathLength > 0 )
    {
      simulation->prey->targetNode = simulation->path[0];
    }
  }

  //  simulation->w = w;
  //  MovePredators( simulation->graphLevels[ 0 ], simulation->gridLevels[ 0 ],
  //  simulation->agents, simulation->numAgents, simulation->nodeCoverStates,
  //      simulation->gridLevels[ 0 ].GetNumNodes(),
  //      simulation->coverElementHeap, simulation->astarNodeHeap,
  //      simulation->w, allNodes );

  //  for ( uint32_t i = 0; i < simulation->numAgents; ++i )
  //  {
  //    FuzzyMoveAgent( simulation->gridLevels[ 0 ], &simulation->agents[ i ],
  //    dt,
  //        &simulation->fuzzy_system_pursuit_angle,
  //        &simulation->fuzzy_system_pursuit_speed,
  //        &simulation->fuzzy_system_avoidance_angle,
  //        &simulation->fuzzy_system_avoidance_speed);
  //  }

  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glUseProgram( shader );
  glm::vec3 centrePoint;
  cameraPosition += cameraVelocity * ( 1.0f / 60.0f );
  if ( followPrey )
  {
    centrePoint.x = -simulation->prey->position.x;
    centrePoint.y = -simulation->prey->position.y;
  }
  else if ( followPredators )
  {
    uint32_t numPredators = 0;
    for ( uint32_t agentIndex = 0; agentIndex < simulation->numAgents;
          ++agentIndex )
    {
      Agent* agent = simulation->agents + agentIndex;
      if ( agent->type == agent_types::PREDATOR )
      {
        centrePoint.x += -agent->position.x;
        centrePoint.y += -agent->position.y;
        numPredators++;
      }
    }
    centrePoint /= (float)numPredators;
  }
  else
  {
    centrePoint.x = -cameraPosition.x;
    centrePoint.y = -cameraPosition.y;
  }

  float aspect =
    (float)simulation->world.m_width / (float)simulation->world.m_height;
  float scale = 1.0f / zoomFactor;
  float w = (float)simulation->world.m_width * scale * 0.5f;
  float h = (float)simulation->world.m_height * scale * 0.5f;
  glm::mat4 projectionMatrix = glm::ortho( -w, w, h, -h, 0.1f, 100.0f );
  glm::mat4 viewMatrix;
  centrePoint.z = -10.0f;
  viewMatrix = glm::translate( viewMatrix, centrePoint );

  glm::mat4 combined = projectionMatrix * viewMatrix;
  RenderGridWorld( simulation->world, combined, scale );
  colourIndex = 0;
  glBindVertexArray( cube.vertexArray );
  glUniformMatrix4fv( matrixLocation, 1, GL_FALSE, glm::value_ptr( combined ) );
  glDrawElements( GL_TRIANGLES, cube.numIndices, GL_UNSIGNED_INT, 0 );
  glUseProgram( shader );
  RenderAgents( combined, simulation->numAgents, simulation->agents );
  glDisable( GL_DEPTH_TEST );
  if ( ( displayedAbstractionLevel >= 1 ) &&
       ( displayedAbstractionLevel < simulation->numAbstractionLevels ) )
  {
    //      for ( uint32_t i = 0;
    //            i < simulation->numAbstractStates[displayedAbstractionLevel];
    //            ++i )
    //      {
    //        DrawCliques( simulation, displayedAbstractionLevel, combined );
    //      }
    DrawAbstractEdges( displayedAbstractionLevel, combined );
  }
  if ( drawGraph )
  {
    DrawGraph( combined );
  }
  if ( drawPath )
  {
    DrawPath( simulation->path, simulation->pathLength,
              simulation->gridLevels[0], combined, glm::vec4( 1, 0, 0, 1 ),
              pathMesh, matrixLocation, colourLocation );
  }
  if ( drawCoverStates )
  {
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    RenderCoverStates( simulation, combined );
  }
  glEnable( GL_DEPTH_TEST );
  glBindVertexArray( 0 );
  glUseProgram( 0 );
}

void GLWidget::resizeGL( int width, int height )
{
  glViewport( 0, 0, width, height );
  windowWidth = width;
  windowHeight = height;
}

void GLWidget::keyPressEvent( QKeyEvent* event )
{
  if ( event->key() == Qt::Key_W )
  {
    cameraVelocity.y = -cameraSpeed;
  }
  else if ( event->key() == Qt::Key_S )
  {
    cameraVelocity.y = cameraSpeed;
  }
  else if ( event->key() == Qt::Key_A )
  {
    cameraVelocity.x = -cameraSpeed;
  }
  else if ( event->key() == Qt::Key_D )
  {
    cameraVelocity.x = cameraSpeed;
  }
}

void GLWidget::keyReleaseEvent( QKeyEvent* event )
{
  if ( event->key() == Qt::Key_W || event->key() == Qt::Key_S )
  {
    cameraVelocity.y = 0.0f;
  }
  else if ( event->key() == Qt::Key_A || event->key() == Qt::Key_D )
  {
    cameraVelocity.x = 0.0f;
  }
}
