#ifndef GRID_WORLD_H
#define GRID_WORLD_H

#include <cstdint>

struct GridCell
{
    bool m_blocked;
    GridCell() : m_blocked( false ) {}
};

struct GridWorld
{
    uint32_t m_width, m_height;
    GridCell *m_cells;
};

struct Agent;

extern float g_predatorSpeed;
extern float g_preySpeed;

extern void InitializePrey( Agent *agent, uint32_t row, uint32_t col );
extern void InitializePredator( Agent *agent, uint32_t row, uint32_t col );

namespace grid_world_utils
{
    extern void Deserialize( GridWorld *world, const char *data, uint32_t size,
        Agent *agents, uint32_t *numAgents, uint32_t maxNumAgents );

    extern bool Deserialize( GridWorld *world, const char *data,
                             uint32_t size );

    extern void CreateGridWorld( GridWorld *world, uint32_t width,
                                 uint32_t height );

    extern void DestroyGridWorld( GridWorld *world );

    extern void GenerateRandomGridWorld( GridWorld *world, float rate );
}
#endif // GRID_WORLD_H
