#include "algorithms.h"

#include <queue>
#include <list>
#include <algorithm>
#include <set>


void CalculateCover( const Graph &graph, const Agent *agents,
    uint32_t numAgents, uint8_t *nodeCoverStates, uint32_t numNodes, CoverElementHeap_t *queue )
{
  queue->Clear();

  for ( uint32_t i = 0; i < numAgents; ++i )
  {
    CoverElement el;
    el.location = agents[ i ].containingNode;
    assert( el.location < numNodes );
    el.agent = agents[ i ];
    el.time = 0.0f;
    queue->Push( el );
  }

  for ( uint32_t i = 0; i < numNodes; ++i )
    nodeCoverStates[ i ] = node_cover_states::NONE;

  while ( !queue->IsEmpty() )
  {
    auto el = queue->Pop();

    assert( el.location < numNodes );
    if ( nodeCoverStates[ el.location ] &
        ( node_cover_states::COVERED | node_cover_states::TARGET_COVERED ) )
    {
      continue;
    }

    if ( el.agent.type == agent_types::PREDATOR )
      nodeCoverStates[ el.location ] |= node_cover_states::COVERED;
    else
      nodeCoverStates[ el.location ] |= node_cover_states::TARGET_COVERED;

    Graph::Connection connections[ 8 ];
    uint32_t n = graph.GetConnections( connections, 8, el.location );

    for ( uint32_t i = 0; i < n; ++i )
    {
      if ( nodeCoverStates[ connections[ i ].m_to ] &
          ( node_cover_states::COVERED | node_cover_states::TARGET_COVERED ) )
      {
        continue;
      }

      auto t = connections[ i ].m_cost / el.agent.speedFac;
      CoverElement e;
      e.location = connections[ i ].m_to;
      assert( e.location < numNodes );
      e.agent = el.agent;
      e.time = t + el.time;
      queue->Push( e );
    }
  }
}

inline uint32_t CalculateDiagonalDistance( uint32_t x, uint32_t y, uint32_t gx,
    uint32_t gy )
{
  auto dx = labs( x - gx );
  auto dy = labs( y - gy );
  return std::max( dx, dy );
}

inline uint32_t CalculateManhattanDistance( int x, int y, int gx,
    int gy )
{
  return labs( gx - x ) + labs( gy - y );
}

static AStarNode CreateAStarNode( uint32_t nodeId, uint32_t goalNode,
    const NodeGrid &grid, const AStarNode *parent )
{
  uint32_t x, y, gx, gy;
  grid.GetNodePosition( &x, &y, nodeId );
  grid.GetNodePosition( &gx, &gy, goalNode );
  AStarNode node;
  node.node = nodeId;
  node.hcost = static_cast< float >( CalculateManhattanDistance(
        x, y, gx, gy ) );
  if ( parent )
  {
    node.path = parent->path;
    node.path.push_back( parent->node );
  }
  node.fcost = node.path.size() + node.hcost; // Doesn't take connection cost into account.
  return node;
}


int AStar( const Graph &graph, const NodeGrid &grid,
    uint32_t *pathNodes, uint32_t maxPathSize,
    uint32_t startNode, uint32_t goalNode, AStarNodeHeap_t *queue,
    const std::unordered_set< uint32_t > &allowedNodes )
{
  std::list< uint32_t > expandedList;

  queue->Clear();

  queue->Push( CreateAStarNode( startNode, goalNode, grid, nullptr ) );

  while ( !queue->IsEmpty() )
  {
    auto node = queue->Pop();

    if ( node.node == goalNode )
    {
      node.path.push_back( node.node );
      if ( node.path.size() + 1 < maxPathSize )
      {
        std::copy_n( node.path.begin(), node.path.size(), pathNodes );
        return node.path.size();
      }
      return -1;
    }

    if ( std::find( expandedList.begin(), expandedList.end(), node.node ) != expandedList.end() )
      continue;
    expandedList.push_back( node.node );

    Graph::Connection connections[ 8 ];
    uint32_t numConnections = graph.GetConnections( connections, 8, node.node );
    for ( uint32_t i = 0; i < numConnections; ++i )
    {
      if ( allowedNodes.count( connections[ i ].m_to ) )
      {
        auto child = CreateAStarNode( connections[ i ].m_to, goalNode,
            grid, &node );
        if ( std::find( expandedList.begin(), expandedList.end(), child.node )
            == expandedList.end() )
        {
          queue->Replace( child );
        }
      }
    }
  }
  return 0;
}

inline bool AreNodesConnected( uint32_t a, uint32_t b, const Graph &graph )
{
  Graph::Connection connections[ 8 ];
  uint32_t n = graph.GetConnections( connections, 8, a );
  for ( uint32_t i = 0; i < n; ++i )
  {
    if ( connections[ i ].m_to == b )
    {
      return true;
    }
  }
  return false;
}

inline bool IsNodeConnectedTo( const Graph &graph, uint32_t node,
    uint32_t *otherNodes, uint32_t numOtherNodes )
{
  Graph::Connection connections[ 8 ];
  uint32_t n = graph.GetConnections( connections, 8, node );
  if ( n < numOtherNodes )
  {
    return false;
  }
  for ( uint32_t i = 0; i < numOtherNodes; ++i )
  {
    bool found = false;
    for ( uint32_t j = 0; j < n; ++j )
    {
      if ( connections[ j ].m_to == otherNodes[ i ] )
      {
        found = true;
        break;
      }
    }
    if ( !found )
    {
      return false;
    }
  }
  return true;
}

inline AbstractState* GetParent( uint32_t node, AbstractState *states,
    uint32_t size )
{
  for ( uint32_t i = 0; i < size; ++i )
  {
    auto state = states + i;
    for ( uint32_t j = 0; j < state->numChildren; ++j )
    {
      if ( state->children[ j ] == node )
      {
        return state;
      }
    }
  }
  return nullptr;
}

bool FindClique( uint32_t node, const Graph &graph,
    uint32_t *clique, uint32_t cliqueSize,
    const std::set< uint32_t > &availableNodes )
{
  Graph::Connection connections[ 8 ];
  auto n = graph.GetConnections( connections, 8, node );
  clique[ 0 ] = node;
  if ( ( n >= 1 ) && ( cliqueSize == 2 ) )
  {
    if ( availableNodes.find( connections[ 0 ].m_to ) != availableNodes.end() )
    {
      clique[ 1 ] = connections[ 0 ].m_to;
      return true;
    }
  }

  uint32_t otherNodes[ 8 ];
  uint32_t numNodes = 0;
  for ( uint32_t i = 0; i < n; ++i )
  {
    if ( availableNodes.find( connections[ i ].m_to ) != availableNodes.end() )
    {
      otherNodes[ numNodes++ ] = connections[ i ].m_to;
    }
  }

  uint32_t connectedNodes[ 8 ];
  connectedNodes[ 0 ] = node;
  uint32_t numConnectedNodes = 1;
  for ( uint32_t i = 0; i < numNodes; ++i )
  {
    if ( IsNodeConnectedTo( graph, otherNodes[ i ], connectedNodes,
          numConnectedNodes ) )
    {
      connectedNodes[ numConnectedNodes++ ] = otherNodes[ i ];
      if ( numConnectedNodes == cliqueSize )
      {
        std::copy_n( connectedNodes, cliqueSize, clique );
        return true;
      }
    }
  }
  return false;
}

uint32_t AbstractGraph( const Graph &graph, const NodeGrid &nodeGrid,
                        AbstractState *states, uint32_t size,
                        Graph *abstractGraph, NodeGrid *abstractGrid, GraphAbstractionTree *tree,
                        uint32_t level )
{
  uint32_t numAbstractNodes = 0;
  std::set< uint32_t > unabstractedStates[ 2 ];
  for ( uint32_t node = 0; node < nodeGrid.GetNumNodes(); ++node )
  {
    unabstractedStates[ 0 ].insert( node );
    unabstractedStates[ 1 ].insert( node );
  }

  tree->InitializeLevel( level, nodeGrid.GetNumNodes() );

  for ( uint32_t cliqueSize = 4; cliqueSize >= 2; --cliqueSize )
  {
    for ( auto node : unabstractedStates[ 0 ] )
    {
      if ( unabstractedStates[ 1 ].find( node ) !=
          unabstractedStates[ 1 ].end() )
      {
        uint32_t clique[ 4 ];
        if ( FindClique( node, graph, clique, cliqueSize,
              unabstractedStates[ 1 ] ) )
        {
          uint32_t row, col;
          nodeGrid.GetNodePosition( &row, &col, node );
          auto abstractNode = abstractGrid->AddNode( row, col );

          assert( numAbstractNodes < size );
          auto abstractState = states + numAbstractNodes++;
          abstractState->numChildren = cliqueSize;
          abstractState->node = abstractNode;

          for ( uint32_t i = 0; i < cliqueSize; ++i )
          {
            abstractState->children[ i ] = clique[ i ];
            tree->AddNode( clique[i], abstractNode, level );
            unabstractedStates[ 1 ].erase( clique[ i ] );
          }
        }
      }
    }
  }

  for ( auto node : unabstractedStates[ 1 ] )
  {
    Graph::Connection connections[ 2 ];
    auto n = graph.GetConnections( connections, 2, node );
    if ( n == 1 )
    {
      auto state = GetParent( connections[ 0 ].m_to, states,
          numAbstractNodes );
      assert( state );
      assert( state->numChildren < MAX_NODES_PER_ABSTRACT_NODE );
      state->children[ state->numChildren++ ] = node;
      tree->AddNode( node, state->node, level );
    }
    else
    {
      uint32_t row, col;
      nodeGrid.GetNodePosition( &row, &col, node );
      auto abstractNode = abstractGrid->AddNode( row, col );
      auto abstractState = states + numAbstractNodes++;
      abstractState->numChildren = 1;
      abstractState->children[ 0 ] = node;
      abstractState->node = abstractNode;
      tree->AddNode( node, abstractNode, level );
    }
  }

  // Calculate abstractState positions
  for ( uint32_t i = 0; i < numAbstractNodes; ++i )
  {
    auto state = states + i;
    for ( uint32_t j = 0; j < state->numChildren; ++j )
    {
      uint32_t row, col;
      nodeGrid.GetNodePosition( &row, &col, state->children[ j ] );
      state->position.x += row;
      state->position.y += col;
    }
    state->position /= state->numChildren;
  }
  Graph::Connection *connections = new Graph::Connection[
          graph.GetNumConnections()];
  auto numConnections = graph.GetAllConnections( connections,
                                                 graph.GetNumConnections() );
  for ( uint32_t i = 0; i < numConnections; ++i )
  {
    auto parent1 = tree->GetParent( connections[i].m_to, level );
    auto parent2 = tree->GetParent( connections[i].m_from, level );
    if ( parent1 != parent2 )
    {
      uint32_t x1, x2, y1, y2;
      abstractGrid->GetNodePosition( &x1, &y1, parent1 );
      abstractGrid->GetNodePosition( &x2, &y2, parent2 );
      auto diff = glm::vec2( x2, y2 ) - glm::vec2( x1, y2 );
      abstractGraph->AddConnection( parent2, parent1, diff.length() );
    }
  }
  delete[] connections;
  return numAbstractNodes;
}

uint32_t FindStartingLevel( uint32_t minLevel, uint32_t maxLevel,
                            uint32_t startNode, uint32_t goalNode,
                            uint32_t *startingNodes, uint32_t *goalNodes,
                            const GraphAbstractionTree &tree )
{
  for ( uint32_t currentLevel = minLevel;
        currentLevel < maxLevel;
        ++currentLevel )
  {
    startNode = tree.GetParent( startNode, currentLevel );
    startingNodes[currentLevel - minLevel] = startNode;
    goalNode = tree.GetParent( goalNode, currentLevel );
    goalNodes[currentLevel - minLevel] = goalNode;
    if ( startNode == goalNode )
    {
      return currentLevel - 1;
    }
  }
  return maxLevel;
}

void AddChildrenToSet( uint32_t parent, uint32_t level,
                       std::unordered_set<uint32_t> *set,
                       const GraphAbstractionTree &tree )
{
  uint32_t children[16];
  uint32_t numChildren = tree.GetChildren( parent, level, children, 16 );
  assert( numChildren < 16 );
  for ( uint32_t i = 0; i < numChildren; ++i )
  {
    set->insert( children[i] );
  }
}
int PartialRefinementAStar( Graph **graphs, NodeGrid *grids,
    uint32_t numLevels, uint32_t *pathNodes, uint32_t maxPathSize,
    uint32_t startNode, uint32_t goalNode, AStarNodeHeap_t *queue,
    const GraphAbstractionTree &tree, std::unordered_set<uint32_t> *searchSpace )
{
  uint32_t startingNodes[MAX_ABSTRACTION_LEVELS];
  std::fill_n( startingNodes, MAX_ABSTRACTION_LEVELS, 0 );
  uint32_t goalNodes[MAX_ABSTRACTION_LEVELS];
  std::fill_n( goalNodes, MAX_ABSTRACTION_LEVELS, 0 );
  uint32_t startingLevel = FindStartingLevel( 0, numLevels, startNode, goalNode,
                                              startingNodes, goalNodes, tree );
  assert( startingLevel < numLevels );
  AddChildrenToSet( startingNodes[startingLevel], startingLevel, searchSpace,
                    tree );
  AddChildrenToSet( goalNodes[startingLevel], startingLevel, searchSpace,
                    tree );
  int pathLength = 0;
  for ( int currentLevel = startingLevel - 1;
        currentLevel >= 0;
        --currentLevel )
  {
    pathLength = AStar( *(graphs[currentLevel]), grids[currentLevel],
                        pathNodes, maxPathSize, startingNodes[currentLevel],
                        goalNodes[currentLevel], queue, *searchSpace );
//    assert( pathLength > 0 );
    searchSpace->clear();
    for ( uint32_t i = 0; i < pathLength; ++i )
    {
      AddChildrenToSet( pathNodes[i], currentLevel, searchSpace, tree );
    }
  }
  return pathLength;
}
