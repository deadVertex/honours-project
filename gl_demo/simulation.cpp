#include "simulation.h"

#include <queue>
#include <cstdio>
#include <cstdarg>
#include <cstdint>
#include <queue>

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/compatibility.hpp"

#include "agent.h"
#include "grid_world.h"
#include "graph.h"
#include "fuzzy_logic.h"
#include "algorithms.h"

static uint32_t CountCover( uint8_t *nodeCoverStates, uint32_t numNodes )
{
    uint32_t count = 0;
    for ( uint32_t i = 0; i < numNodes; ++i )
    {
        if ( nodeCoverStates[ i ] == node_cover_states::COVERED )
            count++;
    }
    return count;
}

static uint32_t FindCoverMaxNode( const Graph &graph, uint8_t *nodeCoverStates,
  uint32_t numNodes, uint32_t *nodes, uint32_t numNodesToTest, Agent *agents,
  uint32_t numAgents, uint32_t agentIndex, uint32_t *maxCover,
  CoverElementHeap_t *queue )
{
    *maxCover = 0;
    uint32_t maxCoverNode = 0;
    uint32_t originalContainingNode = agents[ agentIndex ].containingNode;
    for ( uint32_t i = 0; i < numNodesToTest; ++i )
    {
        agents[ agentIndex ].containingNode = nodes[ i ];
        CalculateCover( graph, agents, numAgents, nodeCoverStates, numNodes, queue );
        uint32_t cover = CountCover( nodeCoverStates, numNodes );
        if ( cover > *maxCover )
        {
            *maxCover = cover;
            maxCoverNode = nodes[ i ];
        }
    }
    agents[ agentIndex ].containingNode = originalContainingNode;
    assert( maxCover > 0 );
    return maxCoverNode;
}

static uint32_t FindNodesInRadius( const Graph &graph, uint32_t centreNode,
  uint32_t radius, uint32_t *nodes, uint32_t numNodes )
{
  std::queue< std::pair< uint32_t, uint32_t > > queue;

  queue.push( std::make_pair( centreNode, 0 ) );

  Graph::Connection connections[ 8 ];

  uint32_t i = 0;
  while ( !queue.empty() )
  {
    uint32_t node = queue.front().first;
    uint32_t depth = queue.front().second;
    queue.pop();

    if ( i < numNodes )
    {
      // Don't want to include the containingNode in the list.
      if ( depth > 0 )
        nodes[ i++ ] = node;
    }
    else
      break;

    auto n = graph.GetConnections( connections, 8, node);

    if ( depth + 1 <= radius )
    {
      for ( uint32_t j = 0; j < n; ++j )
        queue.push( std::make_pair( connections[ j ].m_to, depth + 1 ) );
    }
  }

  return i;
}

static uint32_t FindNodesInSquareRadius( const NodeGrid &grid,
                                       uint32_t centreNode, float radius,
                                       uint32_t *nodes, uint32_t numNodes )
{
    uint32_t cx, cy;
    grid.GetNodePosition( &cx, &cy, centreNode );
  uint32_t r = static_cast< uint32_t >( ceil( radius ) + 1 );
    uint32_t n = 0;

    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx + i, cy, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx, cy + i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx + i, cy + i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        // Its ok if cx - i < 0 because there aren't enough nodes
        // for this to cause any problems.
        if ( grid.GetNodeAtPosition( cx - i, cy, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx, cy - i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx - i, cy - i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx + i, cy - i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        uint32_t node;
        if ( grid.GetNodeAtPosition( cx - i, cy + i, &node ) )
        {
            if ( n < numNodes )
            {
                nodes[ n ] = node;
                n++;
            }
        }
    }

    return n;
}

static uint32_t CalculateCoverForNode( const Graph &graph,
  uint8_t *nodeCoverStates, uint32_t numNodes, Agent *agents,
  uint32_t numAgents, uint32_t agentIndex, uint32_t node, CoverElementHeap_t *queue )
{
    uint32_t originalContainingNode = agents[ agentIndex ].containingNode;

    agents[ agentIndex ].containingNode = node;
    CalculateCover( graph, agents, numAgents, nodeCoverStates, numNodes, queue );
    uint32_t cover = CountCover( nodeCoverStates, numNodes );
    agents[ agentIndex ].containingNode = originalContainingNode;
    return cover;
}

void MovePredators( const Graph &graph, const NodeGrid &grid,
  Agent *agents, uint32_t numAgents, uint8_t *nodeCoverStates, uint32_t numNodes,
  CoverElementHeap_t *queue, AStarNodeHeap_t *astarQueue, float w,
  const std::unordered_set< uint32_t > &allowedNodes )
{
  Agent *prey = nullptr;
  for ( uint32_t j = 0; j < numAgents; ++j )
  {
    if ( agents[ j ].type == agent_types::PREY )
    {
      prey = agents + j;
      break;
    }
  }
  assert( prey );

    for ( uint32_t i = 0; i < numAgents; ++i )
    {
        if ( agents[ i ].type == agent_types::PREDATOR )
        {
            if ( agents[ i ].containingNode != agents[ i ].targetNode )
                continue;

            uint32_t maxCover;
            uint32_t nodes[ 64 ];
            //uint32_t numNodesInRadius2 = FindNodesInSquareRadius( grid,
            //	agents[ i ].containingNode, agents[ i ].speedFac, nodes, 64 );
   //   uint32_t numNodesInRadius = FindNodesInRadius( graph,
   //     agents[ i ].containingNode, agents[ i ].speedFac, nodes, 64 );
   //   assert( numNodesInRadius == numNodesInRadius2 );
            //assert( numNodesInRadius > 0 );

      Graph::Connection connections[ 8 ];
      auto numNodesToTest = graph.GetConnections( connections, 8,
        agents[ i ].containingNode );
      for ( uint32_t j = 0; j < numNodesToTest; ++j )
        nodes[ j ] = connections[ j ].m_to;

            uint32_t maxCoverNode = FindCoverMaxNode( graph, nodeCoverStates, numNodes,
                nodes, numNodesToTest, agents, numAgents, i, &maxCover, queue );

            auto pathLength = AStar( graph, grid, nodes, 64, agents[ i ].containingNode,
                prey->containingNode, astarQueue, allowedNodes );
            assert( pathLength > 0 );

            uint32_t pathCoverNode = nodes[ 1 ];
            uint32_t pathCover = CalculateCoverForNode( graph, nodeCoverStates,
                numNodes, agents, numAgents, i, pathCoverNode, queue );

            assert( maxCover > 0 );
            if ( ( float )pathCover / ( float )maxCover >= 1.0f - w )
            {
                agents[ i ].targetNode = maxCoverNode;
            }
            else
            {
                agents[ i ].targetNode = pathCoverNode;
            }
        }
    }
}

void UpdateContainingNode( const NodeGrid &grid, Agent *agent )
{
  uint32_t x = static_cast< uint32_t >( floor( agent->position.x ) );
  uint32_t y = static_cast< uint32_t >( floor( agent->position.y ) );
    uint32_t node;
    if ( grid.GetNodeAtPosition( x, y, &node ) )
    {
        if ( agent->containingNode == agent->targetNode && agent->targetNode == 0 )
            agent->targetNode = node;
        agent->containingNode = node;
    }
}

static bool CheckPositionForObstacle( const NodeGrid &grid, uint32_t x, uint32_t y,
                                      const glm::vec2 &position, float *minDist,
                                      glm::vec2 *nearestObstacle )
{
    uint32_t node;
    if ( !grid.GetNodeAtPosition( x, y, &node ) )
    {
        glm::vec2 nodePos( x + 0.5f, y + 0.5f );
        auto d = glm::distance(position, nodePos);
        if ( d < *minDist )
        {
            *minDist = d;
            *nearestObstacle = nodePos;
            return true;
        }
    }
    return false;
}


static bool FindNearestObstacle( const NodeGrid &grid, uint32_t node,
                                 const glm::vec2 &position, glm::vec2 *obstaclePosition )
{
    float minDist = 1000.0f;
    glm::vec2 nearestObstacle;

    uint32_t cx, cy;
    grid.GetNodePosition( &cx, &cy, node );
    uint32_t r = 2;

    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx + i, cy, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }

    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx, cy + i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }

    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx + i, cy + i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        // Its ok if cx - i < 0 because there aren't enough nodes
        // for this to cause any problems.
        if ( CheckPositionForObstacle( grid, cx - i, cy, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx, cy - i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx - i, cy - i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx + i, cy - i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }
    for ( uint32_t i = 1; i < r; ++i )
    {
        if ( CheckPositionForObstacle( grid, cx - i, cy + i, position,
            &minDist, &nearestObstacle ) )
        {
            break;
        }
    }

    if ( minDist < 1000.0f )
    {
        *obstaclePosition = nearestObstacle;
        return true;
    }
    return false;
}

void FuzzyMoveAgent( const NodeGrid &grid, Agent *agent, float dt,
                            fuzzy_system_rec *pursuitAngle,
                            fuzzy_system_rec *pursuitSpeed,
                            fuzzy_system_rec *avoidanceAngle,
                            fuzzy_system_rec *avoidanceSpeed)
{
    uint32_t px, py;
    grid.GetNodePosition( &px, &py, agent->targetNode );
    glm::vec2 targetPosition( px + 0.5f, py + 0.5f );

    glm::vec2 diff = targetPosition - agent->position;
    float angleFromTarget = glm::atan2<float,glm::highp>(diff.y, diff.x) *
            (180.0f/glm::pi<float>());
    angleFromTarget = agent->angle - angleFromTarget;

    while ( angleFromTarget < -180.0f )
        angleFromTarget += 360.0f;

    while ( angleFromTarget > 180.0f )
        angleFromTarget -= 360.0f;

    float distanceFromTarget = glm::distance( targetPosition,
                                              agent->position );

    float inputValues[ 2 ];
    inputValues[ 0 ] = angleFromTarget;
    inputValues[ 1 ] = distanceFromTarget;

    //printf( "Angle: %g\n", angleFromTarget );
    //printf( "Distance: %g\n", inputValues[ 1 ] );

    float fuzzyAngle = fuzzy_system( inputValues, *pursuitAngle );
    float fuzzySpeed = fuzzy_system( inputValues, *pursuitSpeed );

    float newAngle = agent->angle + fuzzyAngle;
    if ( newAngle < 0.0f )
        newAngle = 360.0f + newAngle;

    if ( newAngle > 360.0f )
        newAngle = newAngle - 360.0f;


    glm::vec2 obstaclePosition;
    if ( FindNearestObstacle( grid, agent->containingNode, agent->position,
        &obstaclePosition ) )
    {
        float obstacleDistance = glm::distance( agent->position,
                                                obstaclePosition );

        if ( obstacleDistance < 0.9f )
        {
            glm::vec2 obstDiff = obstaclePosition - agent->position;
            float angleFromObstacle = glm::atan2<float,glm::highp>(
                        obstDiff.y, obstDiff.x) * (180.0f/glm::pi<float>());
            angleFromObstacle = agent->angle - angleFromObstacle;

            while ( angleFromObstacle < -180.0f )
                angleFromObstacle += 360.0f;

            while ( angleFromObstacle > 180.0f )
                angleFromObstacle -= 360.0f;

            inputValues[ in_angle ] = angleFromObstacle;
            inputValues[ in_distance ] = obstacleDistance;

            fuzzyAngle = fuzzy_system( inputValues, *avoidanceAngle );
            fuzzySpeed = fuzzy_system( inputValues, *avoidanceSpeed );

            newAngle = agent->angle + fuzzyAngle;
            //newAngle += fuzzyAngle;
            if ( newAngle < 0.0f )
                newAngle = 360.0f + newAngle;

            if ( newAngle > 360.0f )
                newAngle = newAngle - 360.0f;
        }
    }

    agent->angle = newAngle;
    agent->speed = fuzzySpeed * agent->speedFac;
    //printf( "Speed: %g\n", agent->speed );

  float rads = agent->angle * ( glm::pi<float>() / 180.0f );
    agent->position.x += agent->speed * cosf( rads ) * dt;
    agent->position.y += agent->speed * sinf( rads ) * dt;
}

void DiscreteMoveAgent( const NodeGrid &grid, Agent *agent, float dt )
{
    uint32_t row, col;
    grid.GetNodePosition( &row, &col, agent->targetNode );
    agent->position.x = row + 0.5f;
    agent->position.y = col + 0.5f;
}

int FindNodeFurthestAwayFromPredators( const NodeGrid &grid, uint32_t *nodes,
                                       uint32_t numNodes, Agent *agents,
                                       uint32_t numAgents )
{
    float maxDistSum = 0.0f;
    int furthestNodeIndex = -1;

    for ( uint32_t i = 0; i < numNodes; ++i )
    {
        glm::vec2 nodePosition;
        uint32_t row, col;
        grid.GetNodePosition( &row, &col, nodes[ i ] );
        nodePosition.x = static_cast< float >( row );
        nodePosition.y = static_cast< float >( col );

        float dist = 0;
        for ( uint32_t j = 0; j < numAgents; ++j )
        {
            if ( agents[ j ].type == agent_types::PREDATOR )
            {
                dist += glm::distance( nodePosition,
                    agents[ j ].position );
            }
        }

        if ( dist > maxDistSum )
        {
            maxDistSum = dist;
            furthestNodeIndex = i;
        }
    }

    return furthestNodeIndex;
}

bool CheckForInterception( Agent *agents, uint32_t numAgents )
{
    Agent *prey = nullptr;
    for ( uint32_t i = 0 ; i < numAgents; ++i )
    {
        if ( agents[ i ].type == agent_types::PREY )
            prey = agents + i;
    }

    assert( prey );
    //float agentRadius = 0.25f;

    for ( uint32_t i = 0; i < numAgents; ++i )
    {
        if ( agents[ i ].type == agent_types::PREDATOR )
        {
      if ( agents[ i ].containingNode == prey->containingNode )
        return true;
            /*if ( glm::distance( prey->position,
                agents[ i ].position ) < agentRadius )
            {
                return true;
            }*/
        }
    }

    return false;
}

void GenerateGraphFromGridWorld( Graph *graph, NodeGrid *grid,
                                 const GridWorld &world )
{
  for ( uint32_t y = 0; y < world.m_height - 1; ++y )
  {
    for ( uint32_t x = 0; x < world.m_width - 1; ++x )
    {
      if ( !world.m_cells[ x + ( y * world.m_width ) ].m_blocked )
      {
        uint32_t to;
        uint32_t from = grid->AddNode( x, y );
        if ( x > 0 )
        {
          if ( grid->GetNodeAtPosition( x - 1, y, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
        if ( y > 0 )
        {
          if ( grid->GetNodeAtPosition( x, y - 1, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
        if ( x > 0 && y > 0 )
        {
          if ( grid->GetNodeAtPosition( x - 1, y - 1, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
        if ( x < world.m_width - 1 && y > 0 )
        {
          if ( grid->GetNodeAtPosition( x + 1, y - 1, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
      }
    }
  }
}

bool
InitializeSimulation( Simulation *simulation, const char *config )
{
    auto f = fopen( config, "rb" );
    if ( !f )
    {
      char str[ 1024 ];
      snprintf( str, 1024, "Could not find file %s.", config );
      return false;
    }
    fseek( f, 0, SEEK_END );
    auto size = ftell( f );
    fseek( f, 0, SEEK_SET );
    assert( size );
    char *mapData = new char[ size ];
    fread( mapData, 1, size, f );
    fclose( f );

//    grid_world_utils::Deserialize( &simulation->world, mapData, size,
//        simulation->agents, &simulation->numAgents, 10 );
    grid_world_utils::Deserialize( &simulation->world, mapData, size );
    simulation->numAgents = 4;
    InitializePredator( simulation->agents + 0, 3, 3 );
    InitializePredator( simulation->agents + 1, 3, 2 );
    InitializePredator( simulation->agents + 2, 3, 1 );
    InitializePrey( simulation->agents + 3, 1, 1 );
    delete[] mapData;

    simulation->graphLevels[0] = new Graph();
    GenerateGraphFromGridWorld(simulation->graphLevels[0],
        &simulation->gridLevels[ 0 ], simulation->world);

    simulation->tree.InitializeLevel( 0,
                                      simulation->gridLevels[0].GetNumNodes() );

    initFuzzySystemTargetPursuitAngle(
        &simulation->fuzzy_system_pursuit_angle );
    initFuzzySystemTargetPursuitSpeed(
        &simulation->fuzzy_system_pursuit_speed );

    initFuzzySystemAvoidanceAngle( &simulation->fuzzy_system_avoidance_angle );
    initFuzzySystemAvoidanceSpeed( &simulation->fuzzy_system_avoidance_speed );

    simulation->nodeCoverStates = new uint8_t[
      simulation->gridLevels[ 0 ].GetNumNodes() ];

    for ( uint32_t i = 0; i < simulation->numAgents; ++i )
    {
        if ( simulation->agents[ i ].type == agent_types::PREY )
        {
            simulation->prey = &simulation->agents[ i ];
            break;
        }
    }

    std::random_device rd;
    std::default_random_engine engine( (uint32_t)time( NULL ) );

    std::uniform_int_distribution<int> distribution(0,
        simulation->gridLevels[ 0 ].GetNumNodes() - 1 );
    simulation->numFleeBeacons = 30;
    for ( uint32_t i = 0; i < simulation->numFleeBeacons; ++i )
    {
        simulation->fleeBeacons[ i ] = distribution( engine );
    }

    simulation->coverElementHeap = new CoverElementHeap_t();
    simulation->astarNodeHeap = new AStarNodeHeap_t();
    simulation->w = 0.5f;

    uint32_t i = 1;
    for ( ; i < MAX_ABSTRACTION_LEVELS; ++i )
    {
      simulation->abstractStates[i] = new AbstractState[MAX_ABSTRACT_STATES];
      simulation->graphLevels[i] = new Graph();
      simulation->numAbstractStates[i] = AbstractGraph(
          *simulation->graphLevels[i - 1],
          simulation->gridLevels[i - 1],
          simulation->abstractStates[i], MAX_ABSTRACT_STATES,
          simulation->graphLevels[i], &simulation->gridLevels[i],
          &simulation->tree, i - 1 );
      if ( simulation->numAbstractStates[i] == 1 )
      {
        break;
      }
    }
    assert( i < MAX_ABSTRACTION_LEVELS );
    simulation->numAbstractionLevels = i + 1;
    return true;
}
