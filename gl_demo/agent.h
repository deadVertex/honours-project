#ifndef AGENT_H
#define AGENT_H

#include <cstdint>

#include "glm/vec2.hpp"

struct Agent
{
  glm::vec2 position;
  float speed, angle;
    float speedFac;
  uint8_t type;
  uint32_t containingNode, targetNode;
};

namespace node_cover_states
{
  enum Enum
  {
    NONE = 0,
    COVERED = 1 << 0,
    TARGET_COVERED = 1 << 1
  };
}

namespace agent_types
{
  enum
  {
    PREDATOR,
    PREY,
  };
}

#endif // AGENT_H
