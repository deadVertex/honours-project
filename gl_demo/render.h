#ifndef RENDER_H
#define RENDER_H

#define GLEW_STATIC
#include "GL/glew.h"

#include <glm/glm.hpp>

#include "simulation.h"
struct GLBufferedMesh
{
    GLuint vertexBuffer, vertexArray, numVertices;
};

struct GLIndexedMesh
{
    GLuint vertexArray, vertexBuffer, indexBuffer, numIndices;
};

typedef void ( *ShaderErrorCallback_t )( const char * );

extern GLuint CreateShader( const char *vertexSource,
                            const char *fragmentSource,
                            ShaderErrorCallback_t callback );

extern void DeleteShader( GLuint program );

extern GLBufferedMesh CreateTriangle();

extern GLBufferedMesh CreateQuad();

extern GLIndexedMesh CreateCube();

extern GLIndexedMesh CreateCircle( float radius, int segments, bool fill );

extern GLIndexedMesh CreateGraphNodesVisualization(
        const Graph &graph, const NodeGrid &grid );

extern GLIndexedMesh CreateGraphConnectionsVisualization(
        const Graph &graph, const NodeGrid &grid );

extern void DeleteMesh( GLBufferedMesh mesh );

extern void DrawPath( uint32_t *path, uint32_t pathLength,
               const NodeGrid &grid, const glm::mat4 &viewProjection,
               const glm::vec4 &colour, GLBufferedMesh pathMesh,
               GLint matrixLocation, GLint colourLocation );

struct LineBuffer
{
    uint32_t maxLines;
    glm::vec3 *vertices;
};

struct StreamingVertexBuffer
{
    uint32_t maxVertices, numVertices;
    GLuint vertexArray, vertexBuffer;
};

extern StreamingVertexBuffer CreateStreamingVertexBuffer(
        uint32_t maxVertices );

extern GLBufferedMesh CreateBufferedMesh( glm::vec3 *vertices,
                                          uint32_t numVertices );
extern GLBufferedMesh CreateBufferedMesh( float *vertices, uint32_t vertexSize,
                                          uint32_t numVertices );
extern GLIndexedMesh CreateIndexedMesh( glm::vec3 *vertices, uint32_t numVertices,
                                        uint32_t *indices, uint32_t numIndices );
#endif // RENDER_H
