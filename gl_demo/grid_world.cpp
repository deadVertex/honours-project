#include "grid_world.h"

#include <cassert>
#include <cstdio>
#include <cstring>
#include <random>
#include <list>

#include "agent.h"

void InitializePredator( Agent *agent, uint32_t row, uint32_t col )
{
    agent->speed = 0.0f;
    agent->speedFac = g_predatorSpeed;
    agent->type = agent_types::PREDATOR;
    agent->position.x = static_cast< float >( row ) + 0.5f;
    agent->position.y = static_cast< float >( col ) + 0.5f;
    agent->containingNode = 0;
    agent->targetNode = 0;
    agent->angle = 0.0f;
}

void InitializePrey( Agent *agent, uint32_t row, uint32_t col )
{
    agent->speed = 0.0f;
    agent->speedFac = g_preySpeed;
    agent->type = agent_types::PREY;
    agent->position.x = static_cast< float >( row ) + 0.5f;
    agent->position.y = static_cast< float >( col ) + 0.5f;
    agent->containingNode = 0;
    agent->targetNode = 0;
    agent->angle = 0.0f;
}

namespace grid_world_utils
{
    void Deserialize( GridWorld *world, const char *data, uint32_t size,
        Agent *agents, uint32_t *numAgents, uint32_t maxNumAgents )
    {
        char *end;
        uint32_t h = strtol( data, &end, 10 );
        uint32_t w = strtol( end, &end, 10 );
        assert( w > 0 && h > 0 );
        world->m_width = w;
        world->m_height = h;
        world->m_cells = new GridCell[ w * h ];

        uint32_t start = end - data + 1;
        assert( w * h + start < size );
        uint32_t i = start;
        *numAgents = 0;
        for ( uint32_t y = 0; y < h; ++y, i += 1 )
        {
            assert( *numAgents< maxNumAgents );
            for ( uint32_t x = 0; x < w; ++x, ++i )
            {
                uint32_t cellIndex = x + ( y * w );
                if ( data[ i ] == '1' )
                    world->m_cells[ cellIndex ].m_blocked = true;
                else if ( data[ i ] == '2' )
                {
                    InitializePredator( agents + *numAgents, x, y );
                    (*numAgents)++;
                }
                else if ( data[ i ] == '3' )
                {
                    InitializePrey( agents + *numAgents, x, y );
                    (*numAgents)++;
                }
            }
        }
    }

    const char* FindFirstNewline( const char *data )
    {
      for ( uint32_t i = 0; data[ i ] != '\0'; ++i )
      {
        if ( data[ i ] == '\n' )
        {
          return data + i;
        }
      }
      return nullptr;
    }

    bool Deserialize( GridWorld *world, const char *data, uint32_t size )
    {
      char key[ 20 ];
      char type[ 20 ];
      int width, height;
      sscanf( data, "%s %s", key, type );
      if ( strcmp( "type", key ) )
      {
        // Error expected 'type' as first key in map header.
        return false;
      }
      if ( strcmp( "octile", type ) )
      {
        // Error: only octile connectivity is supported.
        return false;
      }

      data = FindFirstNewline( data );
      if ( !data ) { return false; }
      data++;
      sscanf( data, "%s %d", key, &height );
      if ( strcmp( "height", key ) )
      {
        // Error expected 'height' as the second key in the map header.
        return false;
      }

      data = FindFirstNewline( data );
      if ( !data ) { return false; }
      data++;
      sscanf( data, "%s %d", key, &width );
      if ( strcmp( "width", key ) )
      {
        // Error expected 'width' as the third key in the map header.
        return false;
      }
      data = FindFirstNewline( data );
      if ( !data ) { return false; }
      data++;
      if ( width <= 0 || height <= 0 )
      {
        // Error: Invalid map dimensions.
        return false;
      }
      data = FindFirstNewline( data );
      data++;
      world->m_width = width + 1;
      world->m_height = height;
      world->m_cells = new GridCell[ world->m_width * world->m_height ];
      for ( int row = 0; row < height; ++row )
      {
        for ( int col = 0; col < width; ++col )
        {
          uint32_t cellIndex = col + ( row * width );
          if ( data[ cellIndex ] == '@' )
          {
            world->m_cells[ cellIndex ].m_blocked = true;
          }
        }
      }
      return true;
    }

    void CreateGridWorld( GridWorld *world, uint32_t width, uint32_t height )
    {
        world->m_width = width;
        world->m_height = height;
        world->m_cells = new GridCell[ width * height ];
    }

    void DestroyGridWorld( GridWorld *world )
    {
        delete[] world->m_cells;
    }

    void GenerateRandomGridWorld( GridWorld *world, float rate )
    {
        std::default_random_engine engine;
        std::bernoulli_distribution distribution( rate );

        for ( uint32_t i = 0; i < world->m_height; ++i )
        {
            for ( uint32_t j = 0; j < world->m_width; ++j )
            {
                world->m_cells[ j + ( i * world->m_width ) ].m_blocked =
                    distribution( engine );
            }
        }
    }
}
